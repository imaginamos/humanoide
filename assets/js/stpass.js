$(document).ready(function() {
$('.passwordregister').keyup(function() {
$('#result').html(checkStrength($('.passwordregister').val()))
})
function checkStrength(password) {
var strength = 0
if (password.length < 6) {
$('#result').removeClass();
$('#result').addClass('short');
$("#resultprogress").css("display","inline-block");
$("#resultprogress .progress-bar").attr("aria-valuenow","33");
$("#resultprogress .progress-bar").attr("style","width: 33%");
$("#resultprogress .progress-bar").removeClass("progress-bar-success");
$("#resultprogress .progress-bar").removeClass("progress-bar-danger");
$("#resultprogress .progress-bar").removeClass("progress-bar-warning");
$("#resultprogress .progress-bar").addClass("progress-bar-danger");
return 'Demasiado corto'
}
if (password.length > 7) strength += 1
// If password contains both lower and uppercase characters, increase strength value.
if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
// If it has numbers and characters, increase strength value.
if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
// If it has one special character, increase strength value.
if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
// If it has two special characters, increase strength value.
if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
// Calculated strength value, we can return messages
// If value is less than 2
if (strength < 2) {
$('#result').removeClass();
$('#result').addClass('weak');
$("#resultprogress").css("display","inline-block");
$("#resultprogress .progress-bar").attr("aria-valuenow","33");
$("#resultprogress .progress-bar").attr("style","width: 33%");
$("#resultprogress .progress-bar").removeClass("progress-bar-success");
$("#resultprogress .progress-bar").removeClass("progress-bar-danger");
$("#resultprogress .progress-bar").removeClass("progress-bar-warning");
$("#resultprogress .progress-bar").addClass("progress-bar-danger");
    
return 'Débil'
} else if (strength == 2) {
$('#result').removeClass();
$('#result').addClass('good');
$("#resultprogress").css("display","inline-block");
$("#resultprogress .progress-bar").attr("style","width: 66%");
$("#resultprogress .progress-bar").removeClass("progress-bar-success");
$("#resultprogress .progress-bar").removeClass("progress-bar-danger");
$("#resultprogress .progress-bar").removeClass("progress-bar-warning");
$("#resultprogress .progress-bar").addClass("progress-bar-warning");
return 'Bien'
} else {
$('#result').removeClass();
$('#result').addClass('strong');
$("#resultprogress").css("display","inline-block");
$("#resultprogress .progress-bar").attr("style","width: 100%");
$("#resultprogress .progress-bar").removeClass("progress-bar-success");
$("#resultprogress .progress-bar").removeClass("progress-bar-danger");
$("#resultprogress .progress-bar").removeClass("progress-bar-warning");
$("#resultprogress .progress-bar").addClass("progress-bar-success");
return 'Perfecto'
}
}
});