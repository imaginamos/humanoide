$( document ).ready(function() {

        var submitButton12 = $('.registrarme');
        submitButton12.click(function(e){

        e.preventDefault();
                var ajaxform = "#register-form";
                var name = $(ajaxform + " input[name=nombre_o_razon_social_text]").val();
                var email = $(ajaxform + " input[name=email_text]").val();
                var phone = $(ajaxform + " input[name=telefono_number]").val();
                var pass = $(ajaxform + " input[name=password_text]").val();
                var pass2 = $(ajaxform + " input[name=password_confirm]").val();
                var terminosCondiciones = $(ajaxform + " input[name=terminosCondiciones]")[0].checked;
                var politicaTratamientos = $(ajaxform + " input[name=politicaTratamientos]")[0].checked;

                var atpos = email.indexOf("@");
                var dotpos = email.lastIndexOf(".");
                


        if (name == ""){
        	var father = $(ajaxform + " input[name=nombre_o_razon_social_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
        	$(ajaxform + " input[name=nombre_o_razon_social_text]").focus();
        }
        else if (email == ""){
        	var father = $(ajaxform + " input[name=email_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
        	$(ajaxform + " input[name=email_text]").focus();
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            var father = $(ajaxform + " input[name=email_text]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=email_text]").focus();
        }
        else if (phone == ""){
            var father = $(ajaxform + " input[name=telefono_number]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=telefono_number]").focus();
        }
        else if (pass == ""){
            var father = $(ajaxform + " input[name=password_text]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=password_text]").focus();
        }
        else if (pass2 == ""){
            var father = $(ajaxform + " input[name=password_confirm]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=password_confirm]").focus();
        }
        else if (pass != pass2){
            var father = $(ajaxform + " input[name=password_confirm]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=password_confirm]").focus();
        }
        else if (terminosCondiciones == false){
            var father = $(ajaxform + " input[name=terminosCondiciones]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=terminosCondiciones]").focus();
        }
        else if (politicaTratamientos == false){
            var father = $(ajaxform + " input[name=politicaTratamientos]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=politicaTratamientos]").focus();
        }
        else {

	        $.ajax({
	        		type: 'post',
	                cache: false,
	                //dataType: 'json',
	                url: $(ajaxform).attr('action'),
	                data: $(ajaxform).serialize(),
	                beforeSend: function(){
	                	$(ajaxform).find(".msnGood").fadeOut( "slow" );
                        $(ajaxform).find(".msnWait").fadeIn( "slow" );
                        $(ajaxform).find(".msnWait").attr('style', 'display: block;');
	                },
	                success: function(data){

        				$(ajaxform).find(".msnWait").fadeOut( "slow" );
                        
                        var data = JSON.parse(data);
                        if (data[0].status === 'error') {
                            $(ajaxform).find('.sending').html('<label style="color: red">' + data[0].msg + '</label>');
                        } else {
                            $(ajaxform).find('.sending').html('<label>' + data[0].msg + '</label>');
                             location.reload(true);
                        }
	                }

	        });
	    }
	     
    });

});