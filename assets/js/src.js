$( document ).ready(function() {

    $(".selectpickerCiudad").chosen().change(function() {
        window.location.href = $(".baseurl")[0].innerHTML+"proveedor?ciudad="+$(this).val();
    });

    $("#searchProveedor").change(function() {
        window.location.href = $(".baseurl")[0].innerHTML+"proveedor?search="+$(this).val();
    });

    $("#searchFAQ").change(function() {
        window.location.href = $(".baseurl")[0].innerHTML+"faq?searchFAQ="+$(this).val();
    });

    $( "#filtroCategoria" ).change(function() {
	  window.location.href = $(".baseurl")[0].innerHTML+"proveedor?categoria="+$(this).val();
	});

	$( "#cerrarSesion" ).click(function() {
		var action = this.attributes.name.value;
        var base = $("#base_url_header")[0].innerHTML;

	  	$.ajax({
    		type: 'post',
            cache: false,
            url: action,
            success: function(data){
                state = JSON.parse(data);
                if (state[0].status == 'success'){
                    window.location.href = base;
                }
            }
    	});
	});

    //checkbox home click
    $( ".checkboxChange" ).click(function() {
        var status = $( this ).attr("data-status");
        var checkName = $( this ).attr("data-checkName");
        if(status == "false"){
            $( this ).attr("style","background-color: #8a247e");
            $( this ).attr("data-status","true");
            $( "input[name='"+checkName+"']" ).val("checked","true");
        }else if(status == "true"){
            $( this ).attr("style","background-color: white");
            $( this ).attr("data-status","false");
            $( "input[name='"+checkName+"']" ).val("checked","false");
        }
    });

});