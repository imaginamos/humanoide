$( document ).ready(function() {

        var submitButton12 = $('.btnSelection');
        submitButton12.click(function(e){

        e.preventDefault();
                var ajaxform = "#contactform";
                var name = $(ajaxform + " input[name=nombre_text]").val();
                var email = $(ajaxform + " input[name=email_text]").val();
                var message = $(ajaxform + " input[name=mensaje_textarea]").val();

                var atpos = email.indexOf("@");
                var dotpos = email.lastIndexOf(".");


        if (name == ""){
        	var father = $(ajaxform + " input[name=nombre_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important;');
        	$(ajaxform + " input[name=nombre_text]").focus();
        }
        else if (email == ""){
        	var father = $(ajaxform + " input[name=    email_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important;');
        	$(ajaxform + " input[name= email_text]").focus();
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            var father = $(ajaxform + " input[name=email_text]").parent();
            father.find(".errorEmail").fadeIn( "slow" );
            father.find(".errorEmail").attr('style', 'display: block !important;');
            $(ajaxform + " input[name= email_text]").focus();
        }
        else if (message == ""){
            var father = $(ajaxform + " input[name=mensaje_textarea]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important;');
            $(ajaxform + " input[name=mensaje_textarea]").focus();
        }
        else {

	        $.ajax({
	        		type: 'post',
	                cache: false,
	                //dataType: 'json',
	                url: $(ajaxform).attr('action'),
	                data: $(ajaxform).serialize(),
	                beforeSend: function(){
                        $(ajaxform).find(".msnGood").fadeOut( "slow" );
                        $(ajaxform).find(".msnWait").fadeIn( "slow" );
                        $(ajaxform).find(".msnWait").attr('style', 'display: block;');
	                },
	                success: function(data){

		                if (data.type == 'error'){
		                }
		                else{
                            $(ajaxform).find(".msnWait").fadeOut( "slow" );
                            $(ajaxform).find(".msnGood").fadeIn( "slow" );
                            $(ajaxform).find(".msnGood").attr('style', 'display: block;');
		                }
	                }

	        });
	    }
	     
    });

});