$( document ).ready(function() {

        var submitButton12 = $('.ingresar');
        submitButton12.click(function(e){

        e.preventDefault();
                var ajaxform = "#login-form";
                var email = $(ajaxform + " input[name=email_text]").val();
                var pass = $(ajaxform + " input[name=password_text]").val();

                var atpos = email.indexOf("@");
                var dotpos = email.lastIndexOf(".");
                


        
        if (email == ""){
        	var father = $(ajaxform + " input[name=email_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important; color: white;');
        	$(ajaxform + " input[name=email_text]").focus();
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            var father = $(ajaxform + " input[name=email_text]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: white;');
            $(ajaxform + " input[name=email_text]").focus();
        }
        else if (pass == ""){
            var father = $(ajaxform + " input[name=password_text]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: white;');
            $(ajaxform + " input[name=password_text]").focus();
        }
        else {

	        $.ajax({
	        		type: 'post',
	                cache: false,
	                //dataType: 'json',
	                url: $(ajaxform).attr('action'),
	                data: $(ajaxform).serialize(),
	                beforeSend: function(){
	                	$(ajaxform).find(".msnGood").fadeOut( "slow" );
                        $(ajaxform).find(".msnWait").fadeIn( "slow" );
                        $(ajaxform).find(".msnWait").attr('style', 'display: block; color:white;');
	                },
	                success: function(data){

        				$(ajaxform).find(".msnWait").fadeOut( "slow" );
                        
                        var data = JSON.parse(data);
                        if (data[0].status === 'error') {
                            $(ajaxform).find('.sending').html('<label style="color: white">' + data[0].msg + '</label>');
                        } else {
                            $(ajaxform).find('.sending').html('<label>' + data[0].msg + '</label>');
                            window.location.reload();
                        }
	                }

	        });
	    }
	     
    });

});