$( document ).ready(function() {
	var submitService = $('#addService');
	var submitSede = $('#addSede');
	var ajaxform = "#addProveedorForm";

    submitService.click(function(){
    	var categoriaId = $(ajaxform + " select[name=categoria]").val();
    	var serviciosId = $(ajaxform + " select[name=servicio]").val();
    	var desde = $(ajaxform + " input[name=desdePrice]").val();
    	var hasta = $(ajaxform + " input[name=hastaPrice]").val();
        var nameService = $(ajaxform + " input[name=nameService]").val();

        if (categoriaId == null){
            validateSelectTwoFathers(ajaxform,"select","categoria",".empty");
        }else if (serviciosId == null){
            validateSelectTwoFathers(ajaxform,"select","servicio",".empty");
        }else if (desde == ""){
            validateInput(ajaxform,"input","desdePrice",".empty");
        }else if (hasta == ""){
            validateInput(ajaxform,"input","hastaPrice",".empty");
        }else if (nameService == ""){
            validateInput(ajaxform,"input","nameService",".empty");
        }else{

            var categoriaText = $(ajaxform + " select[name=categoria] option[value="+categoriaId+"]")[0].innerHTML;
            var serviciosText = $(ajaxform + " select[name=servicio] option[value="+serviciosId+"]")[0].innerHTML;

        	//add service to list
        	$("#contentServices").append("<tr class='serviceOK'><td name="+categoriaId+" class='txt_center'>"+categoriaText+"</td><td name="+serviciosId+" class='txt_center'>"+serviciosText+"</td><td class='txt_center'>$"+desde+" - $"+hasta+"</td><td class='txt_center'>"+nameService+"</td><td class='txt_center'><a class='close tooltipped actualizarService' data-position='top' data-delay='50' data-tooltip='Actualizar'><i class='material-icons'>edit</i></a><br><a class='close tooltipped deleteservice' data-position='top' data-delay='50' data-tooltip='Eliminar'><i class='material-icons'>close</i></a></td></tr>");

        	$(".tooltipped").tooltip();
            $('#notService').fadeOut("slow");
            
        	deleteInputList('serviceOK');

        	$(ajaxform + " input[name=desdePrice]").val("");
        	$(ajaxform + " input[name=hastaPrice]").val("");
            $(ajaxform + " select[name=categoria]").val("");
            $(ajaxform + " select[name=servicio]").val("");
            $(ajaxform + " input[name=nameService]").val("");

            $(".select_custon select").material_select();
            $(".tooltipped").tooltip();

            //actualizar servicio creado
            $(".actualizarService").click(function () {
                var father = $(this).parent().parent();
                
                var price = father[0].children[2].innerHTML.split(" - ");
                $(ajaxform + " input[name=desdePrice]").val(price[0].replace("$",""));
                $(ajaxform + " input[name=hastaPrice]").val(price[1].replace("$",""));
                $(ajaxform + " select[name=categoria]").val(father[0].children[0].attributes.name.value);
                $(ajaxform + " select[name=servicio]").val(father[0].children[1].attributes.name.value);
                $(ajaxform + " input[name=nameService]").val(father[0].children[3].innerHTML);

                $(".select_custon select").material_select();

                $(ajaxform + " #addService").fadeOut("fast");
                $(ajaxform + " #actualizarService").fadeIn("fast");
                $(ajaxform + " #actualizarService").attr("data-index", $(father).index());
            });
        }
    });

     //actualizar servicio
    $("#actualizarService").click(function () {   
        var index = $(this).attr("data-index");

        var categoriaId = $(ajaxform + " select[name=categoria]").val();
        var serviciosId = $(ajaxform + " select[name=servicio]").val();

        var categoriaText = $(ajaxform + " select[name=categoria] option[value="+categoriaId+"]")[0].innerHTML;
        var serviciosText = $(ajaxform + " select[name=servicio] option[value="+serviciosId+"]")[0].innerHTML;

        $("#contentServices")[0].children[index].children[0].attributes.name.value = categoriaId;
        $("#contentServices")[0].children[index].children[0].innerHTML = categoriaText;
        $("#contentServices")[0].children[index].children[1].attributes.name.value = serviciosId;
        $("#contentServices")[0].children[index].children[1].innerHTML = serviciosText;

        $("#contentServices")[0].children[index].children[3].innerHTML = $(ajaxform + " input[name=nameService]").val();

        $("#contentServices")[0].children[index].children[2].innerHTML = "$"+$(ajaxform + " input[name=desdePrice]").val()+" - $"+$(ajaxform + " input[name=hastaPrice]").val();

        

        $(ajaxform + " input[name=desdePrice]").val("");
        $(ajaxform + " input[name=hastaPrice]").val("");
        $(ajaxform + " select[name=categoria]").val("");
        $(ajaxform + " select[name=servicio]").val("");
        $(ajaxform + " input[name=nameService]").val("");

        $(".select_custon select").material_select();
        $(".tooltipped").tooltip();

        $(ajaxform + " #actualizarService").fadeOut("fast");
        $(ajaxform + " #addService").fadeIn("fast");

    });

    submitSede.click(function(){
    	var direccionSede = $(ajaxform + " input[name=direccionSede]").val();
    	var personContactSede = $(ajaxform + " input[name=personContactSede]").val();
        var correo = $(ajaxform + " input[name=correoSede]").val();
    	var tel1Sede = $(ajaxform + " input[name=tel1Sede]").val();
    	var tel2Sede = $(ajaxform + " input[name=tel2Sede]").val();
    	var ciudadSede = $(ajaxform + " input[name=ciudadSede]").val();
    	var nameSede = $(ajaxform + " input[name=nameSede]").val();
        var Latitude = $(ajaxform + " input[name=Latitude]").val();
        var Longitude = $(ajaxform + " input[name=Longitude]").val();
    	var diasSede = $(ajaxform + " select[name=diasSede]").val();
    	var desdeHora = $(ajaxform + " select[name=desdeHora]").val();
    	var hastaHora = $(ajaxform + " select[name=hastaHora]").val();
        var desdeHoraSabado = $(ajaxform + " select[name=desdeHoraSabado]").val();
        var hastaHoraSabado = $(ajaxform + " select[name=hastaHoraSabado]").val();

        var atpos = correo.indexOf("@");
        var dotpos = correo.lastIndexOf(".");

        if (direccionSede == ""){
            validateInput(ajaxform,"input","direccionSede",".empty");
        }else if (personContactSede == ""){
            validateInput(ajaxform,"input","personContactSede",".empty");
        }else if (correo == ""){
            validateInput(ajaxform,"input","correoSede",".empty");
        }else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
             validateInput(ajaxform,"input","correoSede",".error");
        }else if (tel1Sede == ""){
            validateInput(ajaxform,"input","tel1Sede",".empty");
        }else if (ciudadSede == ""){
            validateInput(ajaxform,"input","ciudadSede",".empty");
        }else if (nameSede == ""){
            validateInput(ajaxform,"input","nameSede",".empty");
        }else if (diasSede.length == 0){
            validateSelectTwoFathers(ajaxform,"select","diasSede",".empty");
        }else if (desdeHora == null){
            validateSelectTwoFathers(ajaxform,"select","desdeHora",".empty");
        }else if (hastaHora == null){
            validateSelectTwoFathers(ajaxform,"select","hastaHora",".empty");
        }else if (Latitude == ""){
            validateSelectTwoFathers(ajaxform,"select","Latitude",".empty");
        }else if (Longitude == ""){
            validateInput(ajaxform,"input","Longitude",".empty");
        }else{

        	//add service to list
        	$("#contentSede").append("<tr class='sedeOK'><td><b class='nameSede'>"+nameSede+"</b> <br><b class='telSede'>"+tel1Sede+"</b><b class='tel2Sede' style='display:none;'>"+tel2Sede+"</b></td><td><b class='ciudadSede'>"+ciudadSede+"</b> <br> <b class='dirSede'>"+direccionSede+"</b><b class='personContactSede' style='display:none;'>"+personContactSede+"</b><b style='display:none;'>"+Latitude+"</b><b style='display:none;'>"+Longitude+"</b><b style='display:none;'>"+correo+"</b></td><td><b class='diasSede' name="+diasSede+">"+diasSede+"</b> <br> <b class='desdeHoraSede'>"+desdeHora+"</b> - <b class='hastaHoraSede'>"+hastaHora+"</b> <br> <b>Sabado</b> <br> <b class='desdeHoraSabadoSede'>"+desdeHoraSabado+"</b> - <b class='hastaHoraSabadoSede'>"+hastaHoraSabado+"</b></td><td><a class='close tooltipped actualizarService' data-position='top' data-delay='50' data-tooltip='Actualizar'><i class='material-icons'>edit</i></a><br><a class='close tooltipped deleteservice' data-position='top' data-delay='50' data-tooltip='Eliminar'><i class='material-icons'>close</i></a></td></tr>");

        	
            $('#notSede').fadeOut("slow");

        	deleteInputList('sedeOK');

            $(ajaxform + " input[name=direccionSede]").val("");
            $(ajaxform + " input[name=personContactSede]").val("");
            $(ajaxform + " input[name=correoSede]").val("");
            $(ajaxform + " input[name=tel1Sede]").val("");
            $(ajaxform + " input[name=tel2Sede]").val("");
            $(ajaxform + " input[name=ciudadSede]").val("");
            $(ajaxform + " input[name=nameSede]").val("");
            $(ajaxform + " select[name=diasSede]").val("");
            $(ajaxform + " select[name=desdeHora]").val("");
            $(ajaxform + " select[name=hastaHora]").val("");
            $(ajaxform + " select[name=desdeHoraSabado]").val("");
            $(ajaxform + " select[name=hastaHoraSabado]").val("");

            $(".select_custon select").material_select();
            $(".tooltipped").tooltip();

            //actualizar sede creada
            $(".actualizarService").click(function () {
                var father = $(this).parent().parent();
                $(ajaxform + " input[name=direccionSede]").val(father[0].children[1].children[2].innerHTML);
                $(ajaxform + " input[name=personContactSede]").val(father[0].children[1].children[3].innerHTML);
                $(ajaxform + " input[name=correoSede]").val(father[0].children[1].children[6].innerHTML);
                $(ajaxform + " input[name=tel1Sede]").val(father[0].children[0].children[2].innerHTML);
                $(ajaxform + " input[name=tel2Sede]").val(father[0].children[0].children[3].innerHTML);
                $(ajaxform + " input[name=ciudadSede]").val(father[0].children[1].children[0].innerHTML);
                $(ajaxform + " input[name=nameSede]").val(father[0].children[0].children[0].innerHTML);
                $(ajaxform + " select[name=diasSede]").val(father[0].children[2].children[0].attributes.name.value.split(","));
                $(ajaxform + " select[name=desdeHora]").val(father[0].children[2].children[2].innerHTML);
                $(ajaxform + " select[name=hastaHora]").val(father[0].children[2].children[3].innerHTML);
                $(ajaxform + " select[name=desdeHoraSabado]").val(father[0].children[2].children[7].innerHTML);
                $(ajaxform + " select[name=hastaHoraSabado]").val(father[0].children[2].children[8].innerHTML);
                $(ajaxform + " input[name=Latitude]").val(father[0].children[1].children[4].innerHTML);
                $(ajaxform + " input[name=Longitude]").val(father[0].children[1].children[5].innerHTML);

                var lat = $(ajaxform + " input[name=Latitude]").val();
                var long = $(ajaxform + " input[name=Longitude]").val();

                $(".select_custon select").material_select();

                $(ajaxform + " #addSede").fadeOut("fast");
                $(ajaxform + " #actualizarSede").fadeIn("fast");
                $(ajaxform + " #actualizarSede").attr("data-index", $(father).index());

                marker.setPosition(new google.maps.LatLng(parseFloat(lat), parseFloat(long)));

            });
        }

    });
    
    //actualizar sede
    $("#actualizarSede").click(function () {    
        var index = $(this).attr("data-index");

        $("#contentSede")[0].children[index].children[1].children[2].innerHTML = $(ajaxform + " input[name=direccionSede]").val();
        $("#contentSede")[0].children[index].children[1].children[3].innerHTML = $(ajaxform + " input[name=personContactSede]").val();
        $("#contentSede")[0].children[index].children[1].children[6].innerHTML = $(ajaxform + " input[name=correoSede]").val();
        $("#contentSede")[0].children[index].children[0].children[2].innerHTML = $(ajaxform + " input[name=tel1Sede]").val();
        $("#contentSede")[0].children[index].children[0].children[3].innerHTML = $(ajaxform + " input[name=tel2Sede]").val();
        $("#contentSede")[0].children[index].children[1].children[0].innerHTML = $(ajaxform + " input[name=ciudadSede]").val();
        $("#contentSede")[0].children[index].children[0].children[0].innerHTML = $(ajaxform + " input[name=nameSede]").val();
        $("#contentSede")[0].children[index].children[2].children[0].attributes.name.value = $(ajaxform + " select[name=diasSede]").val();
        $("#contentSede")[0].children[index].children[2].children[2].innerHTML = $(ajaxform + " select[name=desdeHora]").val();
        $("#contentSede")[0].children[index].children[2].children[3].innerHTML = $(ajaxform + " select[name=hastaHora]").val();
        $("#contentSede")[0].children[index].children[2].children[7].innerHTML = $(ajaxform + " select[name=desdeHoraSabado]").val();
        $("#contentSede")[0].children[index].children[2].children[8].innerHTML = $(ajaxform + " select[name=hastaHoraSabado]").val();
        $("#contentSede")[0].children[index].children[1].children[4].innerHTML = $(ajaxform + " input[name=Latitude]").val();
        $("#contentSede")[0].children[index].children[1].children[5].innerHTML = $(ajaxform + " input[name=Longitude]").val();

        $(ajaxform + " input[name=direccionSede]").val("");
        $(ajaxform + " input[name=personContactSede]").val("");
        $(ajaxform + " input[name=correoSede]").val("");
        $(ajaxform + " input[name=tel1Sede]").val("");
        $(ajaxform + " input[name=tel2Sede]").val("");
        $(ajaxform + " input[name=ciudadSede]").val("");
        $(ajaxform + " input[name=nameSede]").val("");
        $(ajaxform + " select[name=diasSede]").val("");
        $(ajaxform + " select[name=desdeHora]").val("");
        $(ajaxform + " select[name=hastaHora]").val("");
        $(ajaxform + " select[name=desdeHoraSabado]").val("");
        $(ajaxform + " select[name=hastaHoraSabado]").val("");

        $(".select_custon select").material_select();
        $(".tooltipped").tooltip();

        $(ajaxform + " #actualizarSede").fadeOut("fast");
        $(ajaxform + " #addSede").fadeIn("fast");

    });

    

    //add proveedor destacado
    $("#btnAddProveedorDestacado").click(function () {
        var baseurl = $('.baseurl')[0].innerHTML;
        $.ajax({
            type: 'post',
            cache: false,
            url: baseurl+"proveedor/addProveedorDestacado",
            data: {proveedor: $('#idProveedor')[0].innerHTML,
            },
            success: function(data){
                state = JSON.parse(data);
                if (state[0].status == 'success'){
                    window.location.href = state[0].href;
                }else if(state[0].status == 'error'){
                    $("#msgErrorProveedorDestacado")[0].innerHTML = state[0].msg;
                }
            }
        });

    });


    function deleteInputList(classPanel) {
    	$(".deleteservice").click(function () {
    		$(this).parent().parent().fadeOut("slow").removeClass(classPanel);
            //validar si no hay sedes creadas cuando estoy creando el proveedor
            if($("#contentSede tr[class=sedeOK]").length == 0){
                $('#notSede').fadeIn("slow");
            }
            //validar si no hay servicios creadas cuando estoy creando el proveedor
            if($("#contentSede tr[class=serviceOK]").length == 0){
                $('#notService').fadeIn("slow");
            }
    	});
    }

    function validateInput(form,type,nameInput,classMsg){
        var father = $(form +" "+type+"[name="+nameInput+"]").parent();
        father.find(classMsg).fadeIn( "slow" );
        father.find(classMsg).attr('style', 'display: block !important; color: #a2228e;');
        $(form +" "+type+"[name="+nameInput+"]").focus();
    }

    function validateSelectTwoFathers(form,type,nameInput,classMsg){
        var father = $(form +" "+type+"[name="+nameInput+"]").parent().parent();
        father.find(classMsg).fadeIn( "slow" );
        father.find(classMsg).attr('style', 'display: block !important; color: #a2228e;');
        $(form +" "+type+"[name="+nameInput+"]").focus();
    }
});