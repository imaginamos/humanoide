$( document ).ready(function() {

    //agragar proveedor
    var submit = $('#btnAddProveedor');
    submit.click(function(){
        var ajaxform = "#addProveedorForm";
        var name = $(ajaxform + " input[name=nombre_text]").val();
        var email = $(ajaxform + " input[name=correo_text]").val();
        var nit = $(ajaxform + " input[name=NIT_number]").val();
        var personContact = $(ajaxform + " input[name=persona_contacto_text]").val();
        
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        if (name == ""){
        	validateInput(ajaxform,"input","nombre_text",".empty");
        }else if (email == ""){
            validateInput(ajaxform,"input","correo_text",".empty");
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
             validateInput(ajaxform,"input","correo_text",".error");
        }else if (nit == ""){
            validateInput(ajaxform,"input","NIT_number",".empty");
        }else if (personContact == ""){
            validateInput(ajaxform,"input","persona_contacto_text",".empty");
        }else if($("#contentSede tr[class=sedeOK]").length == 0){
            $(".msgErrorProveedor")[0].innerHTML = "Debes agregar una sede para continuar";
        }else if($("#contentServices tr[class=serviceOK]").length == 0){
            $(".msgErrorProveedor")[0].innerHTML = "Debes agregar una servicio para continuar";
        }else{ 
            var info = [$(ajaxform).serialize()];

            var sedes = $("#contentSede tr[class=sedeOK]");
            var arraySedes = [];
            for (var i = 0; i < sedes.length; i++) {
                arraySede = {'nombre_text':sedes[i].children[0].children[0].innerHTML,
                    'ciudad_text':sedes[i].children[1].children[0].innerHTML,
                    'direccion_text':sedes[i].children[1].children[2].innerHTML,
                    'telefono_1_number':sedes[i].children[0].children[2].innerHTML,
                    'telefono_2_number':sedes[i].children[0].children[3].innerHTML,
                    'dias_de_atencion_multiselect':sedes[i].children[2].children[0].attributes.name.value,
                    'horario_de_atencion_inicio_text':sedes[i].children[2].children[2].innerHTML,
                    'horario_de_atencion_fin_text':sedes[i].children[2].children[3].innerHTML,
                    'horario_de_atencion_sabado_inicio_text':sedes[i].children[2].children[7].innerHTML,
                    'horario_de_atencion_sabado_fin_text':sedes[i].children[2].children[8].innerHTML,
                    'persona_contacto_text':sedes[i].children[1].children[3].innerHTML,
                    'latitud':sedes[i].children[1].children[4].innerHTML,
                    'longitud':sedes[i].children[1].children[5].innerHTML,
                    'correo_electronico_text':sedes[i].children[1].children[6].innerHTML,

                    };
                arraySedes.push(JSON.stringify(arraySede));
            }
            sedes = JSON.stringify(arraySedes);

            var services = $("#contentServices tr[class=serviceOK]");
            var arrayServices = [];
            for (var i = 0; i < services.length; i++) {
                arrayServicio = {'proveedor_categorias_relation':services[i].children[0].attributes.name.value,
                    'servicios_humanoide_relation': services[i].children[1].attributes.name.value,
                    'precio': services[i].children[2].innerHTML,
                    'nombre_text': services[i].children[3].innerHTML,
                    };
                arrayServices.push(JSON.stringify(arrayServicio));
            }
            services = JSON.stringify(arrayServices);

            var data1 = new FormData();            
            data1.append("proveedor",info);
            data1.append("servicios",services);
            data1.append("sedes",sedes);
            prepareDataform(data1, $(ajaxform +" input[name=file]"), "key_");
            
            $.ajax({
                    
                url: $(ajaxform).attr('action'),
                method: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: data1,


            /*$.ajax({
                   type: 'post',
                   cache: false,
                   url: $(ajaxform).attr('action'),
                   data: {proveedor: info,
                        servicios: services,
                        sedes: sedes
                    },*/
                   success: function(data){
                        state = JSON.parse(data);
                        if (state[0].status == 'success'){
                            window.location.href = state[0].href;
                        }
                   }

           });

	        
	    }
	     
    });

    //actaulizar proveedor
    var updateBtn = $('#btnUpdateProveedor');
    updateBtn.click(function(){

        var ajaxform = "#updateProveedorForm";
        var idProveedor = $(ajaxform + " input[name=idProveedor]").val();
        var name = $(ajaxform + " input[name=nombre_text]").val();
        var email = $(ajaxform + " input[name=correo_text]").val();
        var nit = $(ajaxform + " input[name=NIT_number]").val();
        
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        if (name == ""){
            validateInput(ajaxform,"input","nombre_text",".empty");
        }else if (email == ""){
            validateInput(ajaxform,"input","correo_text",".empty");
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
             validateInput(ajaxform,"input","correo_text",".error");
        }else if (nit == ""){
            validateInput(ajaxform,"input","NIT_number",".empty");
        }else{ 
            var info = [$(ajaxform).serialize()];

            var arraySedes = [];
            for (var m = 0; m < $(".sedeItem").length; m++) {
                arraySede = {'latitud': $("#latitud"+m)[0].value,
                    'longitud': $("#longitud"+m)[0].value,
                    'nombre_text': $("#name"+m)[0].value,
                    'ciudad_text': $("#ciudadSede"+m)[0].value,
                    'direccion_text': $("#direccionSede"+m)[0].value,
                    'telefono_1_number': $("#tel1Sede"+m)[0].value,
                    'telefono_2_number': $("#tel2Sede"+m)[0].value,
                    'dias_de_atencion_multiselect': $("#diasSede"+m)[0].value,
                    'horario_de_atencion_inicio_text': $("#desdeHora"+m)[0].value,
                    'horario_de_atencion_fin_text': $("#hastaHora"+m)[0].value,
                    'horario_de_atencion_sabado_inicio_text': $("#desdeHoraSabado"+m)[0].value,
                    'horario_de_atencion_sabado_fin_text': $("#hastaHoraSabado"+m)[0].value,
                    'persona_contacto_text': $("#personContactSede"+m)[0].value,
                    'correo_electronico_text': $("#correoSede"+m)[0].value,
                    'id': $("#idSede"+m)[0].value,
                    };
                arraySedes.push(JSON.stringify(arraySede));
            }
            sedes = JSON.stringify(arraySedes);

            var arrayServices = [];
            for (var m = 0; m < $(".seviceItem").length; m++) {
                arrayServicio = {'proveedor_categorias_relation':$("#categoria"+m)[0].value,
                    'servicios_humanoide_relation': $("#servicio"+m)[0].value,
                    'precio_desde_number': $("#desdePrice"+m)[0].value,
                    'precio_hasta_number': $("#hastaPrice"+m)[0].value,
                    'nombre_text': $("#nameService"+m)[0].value,
                    'id': $("#idService"+m)[0].value,
                    };
                arrayServices.push(JSON.stringify(arrayServicio));
            }
            services = JSON.stringify(arrayServices);

            var data1 = new FormData();            
            data1.append("proveedor",info);
            data1.append("servicios",services);
            data1.append("sedes",sedes);
            prepareDataform(data1, $(ajaxform +" input[name=file]"), "key_");
            
            $.ajax({
                url: $(ajaxform).attr('action'),
                method: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: data1,
                   success: function(data){
                        state = JSON.parse(data);
                        if (state[0].status == 'success'){
                            window.location.href = state[0].href;
                        }
                   }
           });
        }
    });

    //formulario perfil
    var submit = $('.savePerfilUser');
    submit.click(function(){
        var ajaxform = "#formPerfilUser";
        var name = $(ajaxform + " input[name=nombre_o_razon_social_text]").val();
        var email = $(ajaxform + " input[name=email_text]").val();
        var tipoPersona = $(ajaxform + " select[name=tipo_persona_select]").val();
        var tipoDcto = $(ajaxform + " select[name=tipo_documento_select]").val();
        var numeroDcto = $(ajaxform + " input[name=numero_documento_number]").val();
        var empresa = $(ajaxform + " input[name=empresa_text]").val();
        var direccion = $(ajaxform + " input[name=direccion_text]").val();
        var ciudad = $(ajaxform + " input[name=ciudad_text]").val();
        var departamento = $(ajaxform + " input[name=departamento_text]").val();
        var pais = $(ajaxform + " input[name=pais_text]").val();
        var website = $(ajaxform + " input[name=website_text]").val();
        var telefono = $(ajaxform + " input[name=telefono_number]").val();
        var celular = $(ajaxform + " input[name=celular_number]").val();
        var fax = $(ajaxform + " input[name=fax_number]").val();
        var nombrePersonaContacto = $(ajaxform + " input[name=nombre_persona_contacto_text]").val();
        var cargo = $(ajaxform + " input[name=cargo_text]").val();
        var nameRepresentanteLegal = $(ajaxform + " input[name=nombre_representante_legal_text]").val();
        var numeroDtoRepresentanteLegal = $(ajaxform + " input[name=numero_documento_representante_legal_number]").val();
        var tipoDctoRepresentante = $(ajaxform + " select[name=tipo_documento_representante_select]").val();
        var nombreInfoFacturacion = $(ajaxform + " input[name=informacion_de_facturacion_text]").val();
        var autorretenedor = $(ajaxform + " input[name=autorretenedor_text]").val();
        var nombreResolucion = $(ajaxform + " input[name=numero_de_resolucion_number]").val();
        var fecha = $(ajaxform + " input[name=fecha_text]").val();
        var regimenIVA = $(ajaxform + " input[name=regimen_IVA_text]").val();
        var tipoContribuyente = $(ajaxform + " input[name=tipo_de_contribuyente_text]").val();
        var tipoRegimen = $(ajaxform + " input[name=tipo_de_regimen_text]").val();
        var actividadEconomica = $(ajaxform + " input[name=actividad_economica_text]").val();
        var tarifa = $(ajaxform + " input[name=tarifa_number]").val();
        var codigoCIIU = $(ajaxform + " input[name=codigo_CIIU]").val();
        var actividad = $(ajaxform + " input[name=descripcion_actividad_text]").val();
        
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");

        if (name == ""){
            validateInput(ajaxform,"input","nombre_o_razon_social_text",".empty");
        }else if (email == ""){
            validateInput(ajaxform,"input","email_text",".empty");
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
             validateInput(ajaxform,"input","email_text",".error");
        }
        else if (tipoPersona == null){
            validateSelectTwoFathers(ajaxform,"select","tipo_persona_select",".empty");
        }
        else if (tipoDcto == null){
            validateSelectTwoFathers(ajaxform,"select","tipo_documento_select",".empty");
        }
        else if (numeroDcto == ""){
            validateInput(ajaxform,"input","numero_documento_number",".empty");
        }
        else if (empresa == ""){
            validateInput(ajaxform,"input","empresa_text",".empty");
        }
        else if (direccion == ""){
            validateInput(ajaxform,"input","direccion_text",".empty");
        }
        else if (ciudad == ""){
            validateInput(ajaxform,"input","ciudad_text",".empty");
        }
        else if (departamento == ""){
            validateInput(ajaxform,"input","departamento_text",".empty");
        }
        else if (pais == ""){
            validateInput(ajaxform,"input","pais_text",".empty");
        }
        else if (website == ""){
            validateInput(ajaxform,"input","website_text",".empty");
        }
        else if (telefono == ""){
            validateInput(ajaxform,"input","telefono_number",".empty");
        }
        else if (celular == ""){
            validateInput(ajaxform,"input","celular_number",".empty");
        }
        else if (fax == ""){
            validateInput(ajaxform,"input","fax_number",".empty");
        }
        else if (nombrePersonaContacto == ""){
            validateInput(ajaxform,"input","nombre_persona_contacto_text",".empty");
        }
        else if (cargo == ""){
            validateInput(ajaxform,"input","cargo_text",".empty");
        }
        else if (nameRepresentanteLegal == ""){
            validateInput(ajaxform,"input","nombre_representante_legal_text",".empty");
        }
        else if (tipoDctoRepresentante == null){
            validateSelectTwoFathers(ajaxform,"select","tipo_documento_representante_select",".empty");
        }
         else if (numeroDtoRepresentanteLegal == ""){
            validateInput(ajaxform,"input","numero_documento_representante_legal_number",".empty");
        }
        else if (nombreInfoFacturacion == ""){
            validateInput(ajaxform,"input","informacion_de_facturacion_text",".empty");
        }
        else if (autorretenedor == ""){
            validateInput(ajaxform,"input","autorretenedor_text",".empty");
        }
        else if (nombreResolucion == ""){
            validateInput(ajaxform,"input","numero_de_resolucion_number",".empty");
        }
        else if (fecha == ""){
            validateInput(ajaxform,"input","fecha_text",".empty");
        }
        else if (regimenIVA == ""){
            validateInput(ajaxform,"input","regimen_IVA_text",".empty");
        }
        else if (tipoContribuyente == ""){
            validateInput(ajaxform,"input","tipo_de_contribuyente_text",".empty");
        }
        else if (tipoRegimen == ""){
            validateInput(ajaxform,"input","tipo_de_regimen_text",".empty");
        }
        else if (actividadEconomica == ""){
            validateInput(ajaxform,"input","actividad_economica_text",".empty");
        }
        else if (tarifa == ""){
            validateInput(ajaxform,"input","tarifa_number",".empty");
        }
        else if (codigoCIIU == ""){
            validateInput(ajaxform,"input","codigo_CIIU",".empty");
        }
        else if (actividad == ""){
            validateInput(ajaxform,"input","descripcion_actividad_text",".empty");
        }
        else{ 
            var info = [$(ajaxform).serialize()];

            var data1 = new FormData();
            
            data1.append("info",info);
            prepareDataform(data1, $(ajaxform +" input[name=file]"), "key_");
            
            $.ajax({
                    
                url: $(ajaxform).attr('action'),
                method: 'post',
                cache: false,
                contentType: false,
                processData: false,
                data: data1,
                success: function(data){
                    state = JSON.parse(data);
                    if (state[0].status == 'success'){
                        window.location.href = state[0].href;
                    }
                }

            });
        }
         
    });

    //Update password
    var submit = $('.savePass');
    submit.click(function(){
        var ajaxform = "#updatePass";

        var pass = $(ajaxform + " input[name=password]").val();
        var repeatPass = $(ajaxform + " input[name=password_text]").val();

        if (pass == ""){
            validateInput(ajaxform,"input","password",".empty");
        }else if (repeatPass == ""){
            validateInput(ajaxform,"input","password_text",".empty");
        }else if (repeatPass != pass){
            validateInput(ajaxform,"input","password_text",".error");
        }else{ 
            var info = [$(ajaxform).serialize()];

            $.ajax({
                    type: 'post',
                    cache: false,
                    url: $(ajaxform).attr('action'),
                    data: {pass: info
                    },
                    beforeSend: function(){
                        /*$(ajaxform).find(".msnGood").fadeOut( "slow" );
                        $(ajaxform).find(".msnWait").fadeIn( "slow" );
                        $(ajaxform).find(".msnWait").attr('style', 'display: block;');*/
                    },
                    success: function(data){
                        state = JSON.parse(data);
                        if (state[0].status == 'igual'){
                          $("#msgPass")[0].innerHTML = state[0].msg;
                        }else if(state[0].status == 'success'){
                            window.location.reload();
                        }
                    }

            });
        }
         
    });

    function validateInput(form,type,nameInput,classMsg){
        var father = $(form +" "+type+"[name="+nameInput+"]").parent();
        father.find(classMsg).fadeIn( "slow" );
        father.find(classMsg).attr('style', 'display: block !important; color: #a2228e;');
        $(form +" "+type+"[name="+nameInput+"]").focus();
    }

    function validateSelectTwoFathers(form,type,nameInput,classMsg){
        var father = $(form +" "+type+"[name="+nameInput+"]").parent().parent();
        father.find(classMsg).fadeIn( "slow" );
        father.find(classMsg).attr('style', 'display: block !important; color: #a2228e;');
        $(form +" "+type+"[name="+nameInput+"]").focus();
    }

    function prepareDataform(data, inputFile, nameFiles){
        nameFiles = nameFiles || "file-";
        $.each(inputFile[0].files, function(i, file) {
            data.append(nameFiles+i, file);
        });
    }

});