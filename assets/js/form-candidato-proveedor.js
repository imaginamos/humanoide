$( document ).ready(function() {

        var submitButton12 = $('.btnSelectionProveedor');
        submitButton12.click(function(e){

        e.preventDefault();
                var ajaxform = "#proveedorForm";
                var nameEmpresa = $(ajaxform + " input[name=nombre_empresa_text]").val();
                var tipoDcto = $(ajaxform + " select[name=tipo_documento_select]").val();
                var numDcto = $(ajaxform + " input[name=numero_documento_number]").val();
                var nameRepresentante = $(ajaxform + " input[name=nombre_persona_contacto_text]").val();
                var cargo = $(ajaxform + " input[name=cargo_persona_contacto_text]").val();
                var email = $(ajaxform + " input[name=  email_text]").val();
                var phone = $(ajaxform + " input[name=  telefono_number]").val();
                var ciudadDireccion = $(ajaxform + " input[name=direccion_text]").val();
                var services = $(ajaxform + " select[name=servicios_humanoide_relation]").val();
                var message = $(ajaxform + " input[name=comentario_textarea]").val();

                var atpos = email.indexOf("@");
                var dotpos = email.lastIndexOf(".");

                var arrayCiudadDireccion = ciudadDireccion.split("/");


        if (nameEmpresa == ""){
        	var father = $(ajaxform + " input[name=nombre_empresa_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
        	$(ajaxform + " input[name=nombre_empresa_text]").focus();
        }
        else if (tipoDcto == "nothing"){
            var father = $(ajaxform + " select[name=tipo_documento_select]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " select[name=tipo_documento_select]").focus();
        }
        else if (numDcto == ""){
            var father = $(ajaxform + " input[name=    numero_documento_number]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name= numero_documento_number]").focus();
        }
        else if (nameRepresentante == ""){
            var father = $(ajaxform + " input[name=    nombre_persona_contacto_text]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name= nombre_persona_contacto_text]").focus();
        }
        else if (cargo == ""){
            var father = $(ajaxform + " input[name=    cargo_persona_contacto_text]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name= cargo_persona_contacto_text]").focus();
        }
        else if (email == ""){
        	var father = $(ajaxform + " input[name=    email_text]").parent();
        	father.find(".empty").fadeIn( "slow" );
        	father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
        	$(ajaxform + " input[name= email_text]").focus();
        }
        else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            var father = $(ajaxform + " input[name=    email_text]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name= email_text]").focus();
        }
        else if (phone == ""){
            var father = $(ajaxform + " input[name=    telefono_number]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name= telefono_number]").focus();
        }
        else if (ciudadDireccion == ""){
            var father = $(ajaxform + " input[name=direccion_text]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=direccion_text]").focus();
        }
        else if (arrayCiudadDireccion.length != 2){
            var father = $(ajaxform + " input[name=direccion_text]").parent();
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=direccion_text]").focus();
        }
        else if (services == "nothing"){
            var father = $(ajaxform + " select[name=servicios_humanoide_relation]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " select[name=servicios_humanoide_relation]").focus();
        }
        else if (message == ""){
            var father = $(ajaxform + " input[name=comentario_textarea]").parent();
            father.find(".empty").fadeIn( "slow" );
            father.find(".empty").attr('style', 'display: block !important; color: #a2228e;');
            $(ajaxform + " input[name=comentario_textarea]").focus();
        }
        else {

	        $.ajax({
	        		type: 'post',
	                cache: false,
	                //dataType: 'json',
	                url: $(ajaxform).attr('action'),
	                data: $(ajaxform).serialize(),
	                beforeSend: function(){
	                	$(ajaxform).find(".msnGood").fadeOut( "slow" );
                        $(ajaxform).find(".msnWait").fadeIn( "slow" );
                        $(ajaxform).find(".msnWait").attr('style', 'display: block;');
	                },
	                success: function(data){

		                if (data.type == 'error'){
		                }
		                else{
        					$(ajaxform).find(".msnWait").fadeOut( "slow" );
                            $(ajaxform).find(".msnGood").fadeIn( "slow" );
                            $(ajaxform).find(".msnGood").attr('style', 'display: block;');
		                }
	                }

	        });
	    }
	     
    });

});