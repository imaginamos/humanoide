<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />

  <!-- Stylesheets
  ============================================= -->
  <!-- <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700,800,900|Crete+Round:400italic" rel="stylesheet" type="text/css" /> -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/style.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/dark.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/magnific-popup.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/carousel.css" type="text/css" />
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/nav.css" type="text/css" />

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/responsive.css" type="text/css" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]-->

  <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/settings.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/layers.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/include/rs-plugin/css/navigation.css">


  <!-- Document Title
  ============================================= -->
  <title>Humanoide</title>

  <style>

    .demos-filter {
      margin: 0;
      text-align: right;
    }

    .demos-filter li {
      list-style: none;
      margin: 10px 0px;
    }

    .demos-filter li a {
      display: block;
      border: 0;
      text-transform: uppercase;
      letter-spacing: 1px;
      color: #444;
    }

    .demos-filter li a:hover,
    .demos-filter li.activeFilter a { color: #1ABC9C; }

    @media (max-width: 991px) {
      .demos-filter { text-align: center; }

      .demos-filter li {
        float: left;
        width: 33.3%;
        padding: 0 20px;
      }
    }

    @media (max-width: 767px) {
      .demos-filter li { width: 50%; }
    }

    .revo-slider-emphasis-text {
      font-size: 64px;
      font-weight: 700;
      letter-spacing: -1px;
      font-family: 'Raleway', sans-serif;
      padding: 15px 20px;
      border-top: 2px solid #FFF;
      border-bottom: 2px solid #FFF;
    }

    .revo-slider-desc-text {
      font-size: 20px;
      font-family: 'Lato', sans-serif;
      width: 650px;
      text-align: center;
      line-height: 1.5;
    }

    .revo-slider-caps-text {
      font-size: 16px;
      font-weight: 400;
      letter-spacing: 3px;
      font-family: 'Raleway', sans-serif;
    }
    .tp-video-play-button { display: none !important; }

    .tp-caption { white-space: nowrap; }

  </style>

</head>

<body class="stretched overlay-menu">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <div class="overlay" id="overlay">
      <nav class="overlay-menu">
        <ul>
          <li><a href="#section-about">Quienes somos</a></li>
          <li><a href="#como-funciona">¿Cómo funciona?</a></li>
          <li><a href="#section-provider">Ser Proveedor</a></li>
          <li><a href="#planes">Planes</a></li>
          <li><a href="#section-contact">Contacto</a></li>
        </ul>
      </nav>
    </div>

  <!-- Header
    ============================================= -->
    <header id="header" class="full-header transparent-header" data-sticky-class="not-dark">

      <div id="header-wrap">

        <div class="container clearfix">

          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

          <!-- Logo
          ============================================= -->
          <div id="logo">
            <a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?= base_url() ?>assets/images/logo.png" alt="Canvas Logo"></a>
            <a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?= base_url() ?>assets/images/logo@2x.png" alt="Canvas Logo"></a>
          </div><!-- #logo end -->

          <!-- Primary Navigation
          ============================================= -->
          <a href="#" class="contacto bounceInLeft" data-toggle="modal" data-target=".bs-example-modal-lg"><span class="hidden-xs">Iniciar sesión / Registro</span><i class="icon-users hidden-md hidden-lg"></i></a>
          <a href="#" class="hamburguer bounceInLeft" id="toggle"><i class="icon-untitled icon-menu"></i></a>

        </div>

      </div>

    </header><!-- #header end -->


    <!-- Slider
    ============================================= -->
    <section id="slider" class="revslider-wrap full-screen slider-parallax clearfix">

      <div id="rev_slider_202_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="concept1" style="background-color:#000000;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.1.1RC fullscreen mode -->
        <div id="rev_slider_202_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.1RC">
          <ul>
            <!-- SLIDE  -->
            <li data-index="rs-674" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/concept_bg2-100x100.jpg" data-rotate="0" data-saveperformance="off" data-title="Unlock Power" data-description="">
              <!-- MAIN IMAGE -->
              <img src="<?= base_url()."uploads/files/". $tables["banner"][0]->imagen_file ?>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->
              <div class="flecha"></div>

              <!-- LAYER NR. 1 -->
              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-674-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1000;e:Power2.easeInOut;" data-transform_out="opacity:0;s:1000;s:1000;" data-start="bytrigger" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="keep" style="z-index: 5;background-color:rgba(255, 255, 255, 0.35);border-color:rgba(0, 0, 0, 0);">
              </div>
              <!-- LAYER NR. 2 -->
              <div class="tp-caption Concept-Title-Dark   tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-2" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-65','0','0','0']" data-fontsize="['70','70','50','40']" data-lineheight="['70','70','50','40']" data-width="['none','none','none','400']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-start="1700" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;text-align:center;"><?= $tables["banner"][0]->titulo_imagen_text; ?>
              </div>

              <!-- LAYER NR. 3 -->
              <div class="tp-caption Concept-SubTitle-Dark   tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['55','-65','-57','-54']" data-fontsize="['21','21','15','15']" data-lineheight="['25','25','20','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;"><?= $tables["banner"][0]->subtitulo_imagen_text; ?>
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-5" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','50','43','39']" data-width="280" data-height="1" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:1500;e:Power3.easeOut;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;" data-start="1900" data-responsive_offset="on" style="z-index: 8;background-color:rgba(0, 0, 0, 1.00);border-color:rgba(0, 0, 0, 0.50);">
              </div>

              <!-- LAYER NR. 5 -->
              <div class="tp-caption Concept-MoreBtn-Dark rev-btn  rs-parallaxlevel-2" id="slide-672-layer-7" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['512','452','541','417']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);cursor:pointer;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:2;" data-start="2100" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"startlayer","layer":"slide-672-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-672-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-7","delay":""},{"event":"click","action":"startlayer","layer":"slide-672-layer-9","delay":""},{"event":"click","action":"startlayer","layer":"slide-672-layer-10","delay":""},{"event":"click","action":"startlayer","layer":"slide-672-layer-11","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-12","delay":""}]' data-responsive_offset="on" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 9; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><i class="icon-untitled icon-play-circle"></i>
              </div>

              <!-- LAYER NR. 6 -->
              <div class="tp-caption Concept-Notice-Dark   rs-parallaxlevel-2" id="slide-672-layer-12" data-x="['center','center','center','center']" data-hoffset="['5','5','5','5']" data-y="['top','top','top','top']" data-voffset="['563','513','621','501']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="2300" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 10; white-space: nowrap;text-align:center;">
                <div class="rs-looped rs-slideloop" data-easing="Power1.easeInOut" data-speed="1" data-xs="0" data-xe="0" data-ys="10" data-ye="0"><i style="font-size:30px;" class="icon-angle-up"></i>
                  <br/>INICIAR VIDEO
                </div>
              </div>

              <!-- LAYER NR. 7 -->
              <div class="tp-caption tp-shape tp-shapewrapper " id="slide-672-layer-9" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="opacity:0;s:800;e:Power2.easeInOut;" data-transform_out="opacity:0;s:1200;e:Power2.easeIn;s:1200;e:Power2.easeIn;" data-start="bytrigger" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 11;background-color:rgba(0, 0, 0, 0.85);border-color:rgba(0, 0, 0, 0);">
              </div>

              <!-- LAYER NR. 8 -->
              <div class="tp-caption Concept-Content   tp-resizeme rs-parallaxlevel-2" id="slide-672-layer-6" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['630','550','520','390']" data-width="['1000','800','480','400']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:700;e:Power3.easeInOut;" data-start="bytrigger" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 12; min-width: 1000px; max-width: 1000px; white-space: normal;text-align:center;"><?= $tables["banner"][0]->subtitulo_video_text; ?>
                <br/>
                <br/><a href="#" class="onepager-link">COOL. LET'S GO!</a>
              </div>

              <!-- LAYER NR. 9 -->
              <div class="tp-caption   tp-resizeme tp-videolayer" id="slide-672-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['231','150','200','150']" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:700;e:Power3.easeInOut;" data-start="bytrigger" data-responsive_offset="on" data-vimeoid="139601645" data-videoattributes="title=0&byline=0&portrait=0&api=1" data-videowidth="['640px','640px','480px','360px']" data-videoheight="['360px','360px','270px','203px']" data-videoloop="none" data-end="bytrigger" data-lasttriggerstate="reset" data-autoplay="off" data-volume="100" style="z-index: 13;">
              </div>

              <!-- LAYER NR. 10 -->
              <div class="tp-caption Concept-Title   tp-resizeme" id="slide-672-layer-11" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['150','70','120','40']" data-width="['none','none','none','400']" data-height="none" data-whitespace="['nowrap','nowrap','nowrap','normal']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:700;" data-start="bytrigger" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 14; white-space: nowrap; font-size: 40px; line-height: 40px;text-align:center;"><?= $tables["banner"][0]->titulo_video_text; ?>
              </div>

              <!-- LAYER NR. 11 -->
              <div class="tp-caption Concept-LessBtn rev-btn " id="slide-672-layer-8" data-x="['right','right','right','right']" data-hoffset="['30','30','5','5']" data-y="['top','top','top','top']" data-voffset="['130','130','55','55']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:2;" data-start="bytrigger" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"stoplayer","layer":"slide-672-layer-6","delay":""},{"event":"click","action":"startlayer","layer":"slide-672-layer-7","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-8","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-9","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-10","delay":""},{"event":"click","action":"stopvideo","layer":"slide-672-layer-10","delay":""},{"event":"click","action":"stoplayer","layer":"slide-672-layer-11","delay":""}]' data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-end="bytrigger" data-lasttriggerstate="reset" style="z-index: 15; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box; color: #FFF"><i class="icon-line2-close"></i>
              </div>
            </li>
          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
      </div>
      <!-- END REVOLUTION SLIDER -->
    </section>

    <!-- Content
    ============================================= -->
    <section id="content" class="nobottommargin">

      <div class="content-wrap" style="padding-top:0px;">

      <!-- Modal login
        ============================================= -->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-body">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <!-- <h4 class="modal-title" id="myModalLabel">Modal Heading</h4> -->
                </div>
                <div class="modal-body clearfix">
                  <div class="col_one_third nobottommargin" id="login">

                    <div class="well well-lg nobottommargin form clearfix">
                      <form id="login-form" name="login-form" class="nobottommargin" action="#" method="post">

                        <div class="heading-block center">
                          <h4>Iniciar sesión</h4>
                        </div>

                        <div class="col_full">
                          <label for="login-form-username">Usuario:</label>
                          <input type="text" id="login-form-username" name="login-form-username" value="" class="form-control" />
                        </div>

                        <div class="col_full">
                          <label for="login-form-password">Contraseña:</label>
                          <input type="password" id="login-form-password" name="login-form-password" value="" class="form-control" />
                        </div>

                        <div class="col_full">
                          <a href="#" class="ingresar"></a>
                        </div>

                      </form>
                    </div>

                  </div>

                  <div class="col_two_third col_last nobottommargin" id="registro">


                    <div class="heading-block center">
                      <h4>No tienes una cuenta? Regístrate ahora!</h4>
                    </div>

                    <form id="register-form" name="register-form" class="nobottommargin" action="#" method="post">

                      <div class="col_half">
                        <label for="register-form-name">Nombre:</label>
                        <input type="text" id="register-form-name" name="register-form-name" value="" class="form-control" />
                      </div>

                      <div class="col_half col_last">
                        <label for="register-form-email">E-mail:</label>
                        <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
                      </div>

                      <div class="clear"></div>

                      <div class="col_half">
                        <label for="register-form-username">Usuario:</label>
                        <input type="text" id="register-form-username" name="register-form-username" value="" class="form-control" />
                      </div>

                      <div class="col_half col_last">
                        <label for="register-form-phone">Teléfono:</label>
                        <input type="text" id="register-form-phone" name="register-form-phone" value="" class="form-control" />
                      </div>

                      <div class="clear"></div>

                      <div class="col_half">
                        <label for="register-form-password">Contraseña:</label>
                        <input type="password" id="register-form-password" name="register-form-password" value="" class="form-control" />
                      </div>

                      <div class="col_half col_last">
                        <label for="register-form-repassword">Confirmar contraseña:</label>
                        <input type="password" id="register-form-repassword" name="register-form-repassword" value="" class="form-control" />
                      </div>

                      <div class="clear"></div>

                      <div class="col_full">
                        <a href="#" class="registrarme"></a>
                      </div>

                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div><!-- #modal login end -->

        <div class="section parallax" style="background-image: url('<?= base_url()."uploads/files/". $tables["quienes_somos"][0]->imagen_quienes_somos_file ?>'); padding: 100px 0; margin-top: 0px" data-stellar-background-ratio="0.3" id="section-about">

          <div class="container clearfix">
            <div id="quienes-somos">
              <div class="container">
                <div class="container2">
                  <div class="container3">
                    <div class="heading-block center">
                      <h2><?= $tables["quienes_somos"][0]->titulo_text ?></h2>
                    </div>
                    <?= $tables["quienes_somos"][0]->descripcion_textarea ?>
                  </div>  
                </div>  
              </div>
            </div>

          </div>

        </div>

        <div class="section parallax" id="como-funciona" style="padding: 100px 0; margin: 0px" data-stellar-background-ratio="0.3">

          <div class="container clearfix">

            <div class="heading-block dark center">
              <h2>¿Como Funciona?</h2>
            </div>

            <img src="<?= base_url() ?>assets/images/como-funciona.png" alt="">

          </div>

        </div>

        <div class="section parallax" style="background-image: url('<?= base_url() ?>assets/images/imagen3.png'); padding: 100px 0; margin-top: 0px !important;" data-stellar-background-ratio="0.3">

          <div class="container clearfix">

            <div id="proveedor">
              <div class="container">
                <div class="heading-block center">
                  <h2>¿Quieres ser Proveedor?</h2>
                </div>
                
                <!-- Postcontent
                ============================================= -->
                <div class="postcontent nobottommargin">

                  <div class="contact-widget">

                    <div class="contact-form-result"></div>

                    <form class="nobottommargin" id="proveedorForm" name="proveedorForm" action="<?= base_url(). "Humanoide/createProveedor" ?>" method="post">

                      <div class="form-process"></div>

                      <div class="col_one_third">
                        <label for="template-contactform-name">Nombre de la empresa <small>*</small></label>
                        <input type="text" id="nombre_empresa_text" name="nombre_empresa_text" value="" class="sm-form-control validate required" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_one_third">
                        <label for="template-contactform-email">Tipo de documento <small>*</small></label>
                        <select class="sm-form-control validate" id="tipo_documento_select" name="tipo_documento_select">
                          <option value="nothing">Elije tipo de documento</option>
                          <option value="cedula">Cédula</option>
                          <option value="tarjeta_de_identidad">Tarjeta de identidad</option>
                          <option value="cedula_de_extranjeria">Cédula de extranjeria</option>
                        </select>
                        <label class="empty" style="display:none;">Debes seleccionar una opción para continuar.</label>
                      </div>

                      <div class="col_one_third col_last">
                        <label for="template-contactform-phone">Numero de documento</label>
                        <input type="number" id="numero_documento_number" name="numero_documento_number" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_one_third">
                        <label for="template-contactform-phone">Nombre del representante</label>
                        <input type="text" id="nombre_persona_contacto_text" name="nombre_persona_contacto_text" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_one_third">
                        <label for="template-contactform-phone">Cargo</label>
                        <input type="text" id="cargo_persona_contacto_text" name="cargo_persona_contacto_text" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_one_third col_last">
                        <label for="template-contactform-phone">Correo electronico</label>
                        <input type="text" id="email_text" name="email_text" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                        <label class="error" style="display:none;">Debes ser un correo valido.</label>
                      </div>

                      <div class="clear"></div>

                      <div class="col_one_third">
                        <label for="template-contactform-phone">Teléfono</label>
                        <input type="number" id="telefono_number" name="telefono_number" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_one_third">
                        <label for="template-contactform-phone">Ciudad / Dirección</label>
                        <input type="text" id="direccion_text" name="direccion_text" value="" class="sm-form-control validate" placeholder="Ej: Bogotá / calle 123" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                        <label class="error" style="display:none;">Debes separar la ciudad y la direccion con un /.</label>
                      </div>

                      <div class="col_one_third col_last">
                        <label for="template-contactform-phone">Servicios</label>
                        <select class="sm-form-control validate" id="servicios_humanoide_relation" name="servicios_humanoide_relation">
                          <option value="nothing">Elije un servicio</option>
                          <?php foreach ($tables["servicios_humanoide"] as $servicio) { ?>
                            <option value="<?= $servicio->id ?>"><?= $servicio->name ?></option>
                          <?php  }  ?>
                        </select>
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="clear"></div>

                      <div class="col_full col_last">
                        <label for="template-contactform-phone">Comentario</label>
                        <input type="text" id="comentario_textarea" name="comentario_textarea" value="" class="sm-form-control validate" />
                        <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                      </div>

                      <div class="col_full">
                        <div class="boton-enviar btnSelectionProveedor" style="cursor: pointer;">
                          <i class="icon-untitled icon-noun-555683-cc"></i>
                          <div class="texto">ENVIAR</div>
                        </div>
                      </div>
                        <label class="msnWait" style="display:none;">Un momento, tu mensaje esta enviandose...</label>
                        <label class="msnGood" style="display:none;">Tu mensaje ha sido enviado.</label>

                      <div class="clear"></div>

                    </form>
                  </div>

                </div><!-- .postcontent end -->

              </div>
            </div>

          </div>

        </div>

        <div class="section parallax" id="como-funciona" style="padding: 100px 0; margin: 0px" data-stellar-background-ratio="0.3">

          <div class="container clearfix">

            <div class="heading-block dark center">
              <h2>Selecciona uno de nuestros planes</h2>
            </div>

            <div class="pricing bottommargin clearfix">

              <div class="col-md-4" data-animate="fadeInLeft">

                <div class="pricing-box pricing-box-lat">
                  <div class="pricing-box-2">
                    <div class="pricing-box-3">
                      <div class="pricing-title">
                        <h3>Plan Gratis</h3>
                      </div>
                      <div class="pricing-price">
                        <span class="price-unit">$</span>0
                      </div>
                      <div class="pricing-features">
                        <ul>
                          <li class="pf1">Precio Anual</li>
                          <li class="pf2">$0 <br><span>Precio Mensual</span></li>
                          <li>Dos perfiles<br>Tres proveedores por categoría</li>
                        </ul>
                      </div>
                      <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-sm">COMPRAR</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-md-4" data-animate="fadeInLeft"  data-delay="250">

                <div class="pricing-box best-price">
                  <div class="pricing-box-2">
                    <div class="pricing-box-3">
                      <div class="pricing-title">
                        <h3>Plan Básico</h3>
                      </div>
                      <div class="pricing-price">
                        <span class="price-unit">$</span>900.000
                      </div>
                      <div class="pricing-features">
                        <ul>
                          <li class="pf1">Precio Anual</li>
                          <li class="pf2">$130.000 <br><span>Precio Mensual</span></li>
                          <li>Ocho Ilimitados<br>Proveedores Ilimitados</li>
                        </ul>
                      </div>
                      <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-sm">COMPRAR</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-md-4" data-animate="fadeInLeft">

                <div class="pricing-box pricing-box-lat">
                  <div class="pricing-box-2">
                    <div class="pricing-box-3">
                      <div class="pricing-title">
                        <h3>Plan Premium</h3>
                      </div>
                      <div class="pricing-price">
                        <span class="price-unit">$</span>1.200.000
                      </div>
                      <div class="pricing-features">
                        <ul>
                          <li class="pf1">Precio Anual</li>
                          <li class="pf2">$300.000 <br><span>Precio Mensual</span></li>
                          <li>Perfiles Ilimitados<br>Proveedores Ilimitados</li>
                        </ul>
                      </div>
                      <div class="pricing-action">
                        <a href="#" class="btn btn-danger btn-sm">COMPRAR</a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

            </div>

          </div>

        </div>

        <div class="section parallax bottommargin-lg" style="background-image: url('<?= base_url() ?>assets/images/imagen4.png'); padding: 100px 0; margin-top: 0px" data-stellar-background-ratio="0.3">

          <div class="container clearfix">
            <div class="heading-block center">
              <h2>Contáctanos</h2>
            </div>

            <div id="contactenos" class="col_full clearfix">
              <div class="col-md-8 form">

                <form class="nobottommargin formcontacto" id="contactform" name="contactform" action="<?= base_url(). "Humanoide/createContact" ?>" method="post">

                  <div class="form-process"></div>

                  <div class="col_full">
                    <label for="template-contactform-name">Nombre <small>*</small></label>
                    <input type="text" id="nombre_text" name="nombre_text" class="validate sm-form-control required" />
                    <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                  </div>

                  <div class="col_full">
                    <label for="template-contactform-email">Correo electrónico <small>*</small></label>
                    <input type="email" id="email_text" name="email_text" value="" class="validate required email sm-form-control" />
                    <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                    <label class="error" style="display:none;">Debes ser un correo valido.</label>
                  </div>

                  <div class="col_full col_last">
                    <label for="template-contactform-phone">Comentarios</label>
                    <input type="text" id="mensaje_textarea" name="mensaje_textarea" value="" class="validate sm-form-control" />
                    <label class="empty" style="display:none;">Debes llenar este campo continuar.</label>
                  </div>

                  <div class="col_full">
                    <div class="boton-enviar btnSelection" style="cursor: pointer;">
                      <i class="icon-untitled icon-noun-555683-cc"></i>
                      <div class="texto">ENVIAR</div>
                    </div>
                  </div>
                    <label class="msnWait" style="display:none;">Un momento, tu mensaje esta enviandose...</label>
                    <label class="msnGood" style="display:none;">Tu mensaje ha sido enviado.</label>

                  <div class="clear"></div>

                </form>
              
              </div>

              <div class="col-md-4 info">
                <div class="heading center">
                  <h4>INFORMACIÓN GENERAL</h4>
                </div>
                
                <div class="col_full nobottommargin">

                  <div class="feature-box fbox-plain">
                    <div class="fbox-icon">
                      <a href="#"><i class="icon-untitled icon-noun-555693-cc"></i></a>
                    </div>
                    <h3>Dirección</h3>
                    <p><?= $tables["footer"][0]->direccion_text ?></p>
                  </div>

                </div>

                <div class="col_full nobottommargin">

                  <div class="feature-box fbox-plain">
                    <div class="fbox-icon">
                      <a href="#"><i class="icon-untitled icon-noun-555701-cc"></i></a>
                    </div>
                    <h3>Teléfono</h3>
                    <p><?= $tables["footer"][0]->telefono_number ?></p>
                  </div>

                </div>

                <div class="col_full nobottommargin">

                  <div class="feature-box fbox-plain">
                    <div class="fbox-icon">
                      <a href="#"><i class="icon-untitled icon-noun-555723-cc"></i></a>
                    </div>
                    <h3>Correo Electrónico</h3>
                    <p><?= $tables["footer"][0]->email_text ?></p>
                  </div>

                </div>

              </div>
            </div>
            
          </div>

        </div>


      </div>

    </section><!-- #content end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="<?= base_url() ?>assets/js/template/jquery.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/js/template/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="<?= base_url() ?>assets/js/template/functions.js"></script>

  <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>

  <script type="text/javascript" src="<?= base_url() ?>assets/js/src.js"></script>

  <!-- script formularios -->
  <script type="text/javascript" src="<?= base_url() ?>assets/js/form-contact.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>assets/js/form-proveedor.js"></script>

  <script type="text/javascript">
    var tpj = jQuery;

    var revapi202;
    tpj(document).ready(function() {
      if (tpj("#rev_slider_202_1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev_slider_202_1");
      } else {
        revapi202 = tpj("#rev_slider_202_1").show().revolution({
          sliderType: "standard",
          jsFileLocation: "include/rs-plugin/js/",
          sliderLayout: "fullscreen",
          dottedOverlay: "none",
          delay: 9000,
          navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            onHoverStop: "off",
            touch: {
              touchenabled: "on",
              swipe_threshold: 75,
              swipe_min_touches: 50,
              swipe_direction: "horizontal",
              drag_block_vertical: false
            },
            bullets: {
              enable: true,
              hide_onmobile: false,
              style: "zeus",
              hide_onleave: false,
              direction: "horizontal",
              h_align: "center",
              v_align: "bottom",
              h_offset: 0,
              v_offset: 30,
              space: 5,
              tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title">{{title}}</span>'
            }
          },
          responsiveLevels: [1240, 1024, 778, 480],
          visibilityLevels: [1240, 1024, 778, 480],
          gridwidth: [1240, 1024, 778, 480],
          gridheight: [868, 768, 960, 720],
          lazyType: "none",
          shadow: 0,
          spinner: "off",
          stopLoop: "on",
          stopAfterLoops: 0,
          stopAtSlide: 1,
          shuffle: "off",
          autoHeight: "off",
          fullScreenAutoWidth: "off",
          fullScreenAlignForce: "off",
          fullScreenOffsetContainer: "",
          fullScreenOffset: "",
          disableProgressBar: "on",
          hideThumbsOnMobile: "off",
          hideSliderAtLimit: 0,
          hideCaptionAtLimit: 0,
          hideAllCaptionAtLilmit: 0,
          debugMode: false,
          fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: false,
          }
        });
        revapi202.bind("revolution.slide.onchange",function (e,data) {
          if( $(window).width() > 992 ) {
            if( $('#slider ul > li').eq(data.slideIndex-1).hasClass('dark') ){
              $('#header.transparent-header:not(.sticky-header,.semi-transparent)').addClass('dark');
              $('#header.transparent-header.sticky-header,#header.transparent-header.semi-transparent.sticky-header').removeClass('dark');
              $('#header-wrap').removeClass('not-dark');
            } else {
              if( $('body').hasClass('dark') ) {
                $('#header.transparent-header:not(.semi-transparent)').removeClass('dark');
                $('#header.transparent-header:not(.sticky-header,.semi-transparent)').find('#header-wrap').addClass('not-dark');
              } else {
                $('#header.transparent-header:not(.semi-transparent)').removeClass('dark');
                $('#header-wrap').removeClass('not-dark');
              }
            }
            SEMICOLON.header.logo();
          }
        });
      }


      $( ".validate" ).change(function() {
          var father = $(this).parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $("#proveedorForm input[name=email_text]").change(function() {
          var email = $("#proveedorForm input[name=email_text]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#proveedorForm input[name= email_text]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });

      $("#contactform input[name=email_text]").change(function() {
          var email = $("#contactform input[name=email_text]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#contactform input[name= email_text]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });





    }); /*ready*/
  </script>

</body>
</html>