<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6 no-js" lang="en"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 no-js" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 no-js" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->
<!-- html5.js for IE less than 9 -->
<!--[if lt IE 9]>  <script src="assets/js/lib/html5.js"></script>  <![endif]-->
<html>
  <head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="content-language" content="es" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>HUMANOIDE</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="copyright" content="imaginamos.com" />
    <meta name="date" content="2016" />
    <meta name="author" content="diseño web: imaginamos.com" />
    <meta name="robots" content="All" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="author" type="text/plain" href="humans.txt" />
    <!--Style's-->
    <link href="<?= base_url() ?>assets/css/template/app.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/template/chosen.css" type="text/css" />

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>assets/favicon/apple-icon-57x57.png">

    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url() ?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>assets/favicon/favicon-16x16.png">

    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url() ?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body>

    <section class="preload flex">
        <div id="preloader_3"></div>
    </section>

    <main class="content_all">
      <div class="baseurl" style="display: none;" ><?php echo base_url(); ?></div>
      <header>
        <section class="access">
          <section class="wrapper_l">          
            <a href="index.php" class="logo">
              <img src="<?= base_url() ?>assets/images/logo2.png" alt="">
            </a>       
            <a href="<?= base_url().'Usuario' ?>" class="bg_user">
              <h2 class="name_user"><?php echo $user->nombre_o_razon_social_text; ?></h2>
              <?php if(!empty($user->imagen_perfil_file)){ ?>
                <div class="user relative">            
                  <img src="<?= base_url().'uploads/perfil/'.$user->imagen_perfil_file; ?>" alt="">
                </div> 
               <?php }else{ ?>
                <div class="user relative">            
                  <img src="<?= base_url() ?>assets/images/user.jpg" alt="">
                </div> 
               <?php } ?>
            </a>
            <span id="cerrarSesion" name="<?= base_url().'Usuario/cerrarSesion' ?>" style="cursor:pointer">Cerrar sesion</span>
            <div id="base_url_header" style="display: none;"><?= base_url(); ?></div>
          </section>  
        </section>
        <nav>
          <section class="wrapper_l">        
            <ul>
            <?php if (empty($tab)):
              $tab = "";
            endif ?>
                <li><a href="#" class="ico1"><span></span>Dashboard</a></li>
                <li><a href="#" class="ico2"><span></span> Perfiles</a></li>       
                <li><a href="<?= base_url().'proveedor' ?>" class="ico3">
                <?php if ($tab == "proveedor"){ ?>
                  <span style=" content: ''; position: absolute; bottom: 0; left: 0; width: 100%; height: 3px;background: #a2228e;-webkit-transition: 0.3s; -o-transition: 0.3s;-ms-transition: 0.3s;-moz-transition: 0.3s;transition: 0.3s;"></span> 
                <?php }else{ ?>
                  <span></span>
                <?php } ?>
                Proveedores</a></li>
                <li><a href="<?= base_url().'faq' ?>" class="ico4">
                <?php if ($tab == "faq"){ ?>
                  <span style=" content: ''; position: absolute; bottom: 0; left: 0; width: 100%; height: 3px;background: #a2228e;-webkit-transition: 0.3s; -o-transition: 0.3s;-ms-transition: 0.3s;-moz-transition: 0.3s;transition: 0.3s;"></span> 
                <?php }else{ ?>
                  <span></span>
                <?php } ?> 
                FAQ</a></li>
            </ul>
            <a href="" class="logo animated bounceInDown">
              <img src="<?= base_url() ?>assets/images/logo2.png" alt="">
            </a>       
          </section>
        </nav>
      </header>

      <!-- Modal Cambio contreñas -->
      <section id="edit_pass" class="modal modal-s">
        <div class="modal-header">
          <h1><i class="material-icons">lock_outline</i> CAMBIAR CONTRASEÑA</h1>
        </div>
        <div class="block padd_v">
          <form class="formweb" id="updatePass" action="<?= base_url().'Usuario/editPassword' ?>" >
            <fieldset class="large-12 medium-12 small-12 columns padd_all">
                <div class="input-field">
                  <input class="validate" type="password" name="password" placeholder="Ingresar contraseña"> 
                  <label>CONTRASEÑA</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>           
            </fieldset>
            <fieldset class="large-12 medium-12 small-12 columns padd_all">
                <div class="input-field">
                  <input class="validate" type="password" name="password_text" placeholder="Confirmar nueva contraseña"> 
                  <label>CONFIRMAR CONTRASEÑA</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  <span class="error" style="display:none;">Las contraseñas deben coincidir.</span>
                </div>           
            </fieldset>
            <span id="msgPass"></span>
              <div class="clear"></div>
          </form>
        </div>
        <div class="modal-footer">    
          <a class="btnb waves-effect waves-light right savePass">GUARDAR</a>
          <a href="#!" class="btnb pop modal-action modal-close waves-effect waves-light right">CANCELAR</a>
        </div>

      </section>
      <!-- Fin Modal Cambio contreñas -->