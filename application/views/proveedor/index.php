      <?php echo $header; ?>
      <div class="baseurl" style="display: none;"><?php echo base_url(); ?></div>
      <!-- Inicio Slider -->
      <section class="bg_slider">
        <ul class="carrusel_slider">  
          <li class="item">
            <a href="">
              <img src="<?= base_url() ?>assets/images/slider1.png" alt=""/>
              <div class="cont_text_slider">                  
                <h1>Introducción a proveedores</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur quis tortor vitae nisi scelerisque semper id non lectus. In ac convallis est. Nullam egestas odio vel consequat gravida.</p>
              </div>
              <div class="mask"></div>
            </a>
          </li>
        </ul>        
      </section>
      <!--Fin Slider-->

      <!--a href="<?= base_url() ?>proveedor/viewAddProveedor" class="btn-floating btn-large waves-effect waves-light btn_floating_home tooltipped" data-position="top" data-delay="50" data-tooltip="Nuevo Proveedor" style="position: fixed; top: 80%;"><i class="material-icons">add</i></a-->

      <section class="marcas wow fadeInUp">        
        <div class="wrapper_l padding relative">    
          <a href="<?= base_url() ?>proveedor/viewAddProveedor" class="btn-floating btn-large waves-effect waves-light tooltipped" data-position="top" data-delay="50" data-tooltip="Nuevo Proveedor" style="position: fixed; top: 80%;"><i class="material-icons">add</i></a>   
          <ul class="marcas_slide">
          <?php if (!empty($destacados)) { ?>
            <?php foreach ($destacados as $value) { ?>
              <a href="<?php echo base_url().'proveedor/viewDetalleProveedor?proveedor='.$value->id; ?>">
                <li style="height: 100px; width: 100px;" class="item">              
                  <img style="height: 100px; width: 100px;" src="<?= base_url().'uploads/files/'.$value->imagen_file; ?>" alt="">              
                </li>
              </a>
            <?php } ?>
          <?php } ?>
          
          </ul>
        </div>
      </section>

      <section class="bg_features_h">        
        <nav class="filter_work">
          <div class="wrapper_l padding">
            <ul>
              <li>
                <div class="input-field select_custon">
                  <i class="material-icons">assignment</i>
                  <select id="filtroCategoria">
                      <option value="" disabled selected>Selecciona una categoría</option>
                      <?php foreach ($categorias as $value) { ?>
                        <option class="categoriaFiltro" value="<?php echo $value->id; ?>"><?php echo $value->nombre_text; ?></option>
                      <?php } ?>
                    </select>
                </div>
              </li> 
              <?php if(!empty($ciudades)): ?>
              <li style="width: 25%;">
                <div class="input-field icons">
                  <i class="material-icons">location_on</i>
                  <!--input type="text" placeholder="Ciudad"-->
                  <select data-placeholder="Selecciona una ciudad..." class="selectpickerCiudad">
                    <?php if(!empty($_GET["ciudad"])){ ?>
                      <option value="" disabled >Selecciona una ciudad</option>
                    <?php }else{ ?>
                      <option value="" disabled selected>Selecciona una ciudad</option>
                    <?php } ?>
                    <?php foreach ($ciudades as $city) { ?>
                      <?php if((!empty($_GET["ciudad"])) && ($city->ciudad_text == $_GET["ciudad"])) { ?>
                        <option value="<?php echo($city->ciudad_text) ?>" selected ><?php echo($city->ciudad_text) ?></option>
                      <?php }else{ ?>
                        <option value="<?php echo($city->ciudad_text) ?>" ><?php echo($city->ciudad_text) ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </li> 
              <?php endif ?>
              <li>
                <a href="<?php echo base_url().'proveedor'; ?>">
                  <div class="input-field select_custon">
                    <i class="material-icons">perm_identity</i>
                    <b style="margin-left: 25px;">Mis Proveedores</b>
                  </div>
                </a>
              </li>  
              <li>
                <div class="search icon">
                  <i class="material-icons">search</i>
                  <?php if (!empty($_GET["search"])) { ?>
                    <input type="text" placeholder="Buscar" id="searchProveedor" value="<?php echo $_GET["search"]; ?>">
                  <?php }else{ ?>
                    <input type="text" placeholder="Buscar" id="searchProveedor">
                  <?php } ?>
                </div>
              </li>
            </ul>
          </div>
        </nav>

        <section class="wrapper_l p_b_60">
          <div class="tittle_h padding">
            <?php if(!empty($categoria)){ ?>
              <h2><?php echo $categoria[0]->nombre_text; ?></h2>
            <?php }else{ ?>
              <h2>Mis Proveedores</h2>
            <?php  } ?>
          </div>
          <?php $count = 0; ?>          
          <ul class="features_h">
          <?php for ($i=0; $i < count($proveedor); $i++) { ?>
            <?php if($count < 6): ?>
               <li class="small-12 medium-6 large-4 columns padding">
                <div class="bg_panel">
                  <a href="<?php echo base_url().'proveedor/viewDetalleProveedor?proveedor='.$proveedor[$i]->id; ?>">
                    <div class="imgbg">                  
                      <div class="mask"></div>
                      <?php if (!empty($proveedor[$i]->imagen_file)) { ?>
                        <img src="<?= base_url().'uploads/files/'.$proveedor[$i]->imagen_file; ?>" alt="">
                      <?php }else{ ?>
                        <img src="<?= base_url() ?>assets/images/logo-emp1.png" alt="">
                      <?php } ?>
                    </div>
                    <div class="txt padd_all">
                      <h2><?php echo $proveedor[$i]->nombre_text; ?></h2>
                    </div>
                  </a>
                </div>
              </li>
              <?php $count = $count + 1; ?>
            <?php endif ?>   
          <?php  } ?>
          </ul>

          <div class="clear"></div>

          <ul class="features_h more_products">
          <?php for ($i=6; $i < count($proveedor); $i++) { ?>
            <?php if($count >= 6): ?>
              <li class="small-12 medium-6 large-4 columns padding">
                <div class="bg_panel">
                  <a href="<?php echo base_url().'proveedor/viewDetalleProveedor?proveedor='.$proveedor[$i]->id; ?>">                  
                    <div class="imgbg">                  
                      <div class="mask"></div>
                      <?php if (!empty($proveedor[$i]->imagen_file)) { ?>
                        <img src="<?= base_url().'uploads/files/'.$proveedor[$i]->imagen_file; ?>" alt="">
                      <?php }else{ ?>
                        <img src="<?= base_url() ?>assets/images/logo-emp1.png" alt="">
                      <?php } ?>
                    </div>
                    <div class="txt padd_all">
                      <h2><?php echo $proveedor[$i]->nombre_text; ?></h2>
                    </div>
                  </a>
                </div>
              </li>
              <?php $count = $count + 1; ?>
            <?php endif ?>
          <?php  } ?>
          </ul>

          <a href="javascript:void(0)" class="clic_more clic_more_prod wow fadeInUp">
            <img src="<?= base_url() ?>assets/images/more.png" alt="">
            <h2>VER MÁS</h2>
          </a>

        </section>
      </section>
    

      <?php echo $footer; ?>

      <script type="text/javascript">
        
      </script>   

    </main>

  </body>
</html>
