    <?php echo $header; ?>
    <div id="base_url" style="display:none;"><?php echo(base_url()); ?></div>
      <section class="intert_h animated fadeInUp">
        <div class="tittle">
          <div class="wrapper_l relative">
            <a href="<?= base_url().'proveedor'; ?>" class="back"><i class="material-icons">arrow_back</i> Volver al Listado</a>
            <div class="inline">              
              <h1>Formulario para un nuevo proveedor</h1>
            </div>
          </div>
        </div>
        <form id="addProveedorForm" action="<?php echo base_url().'proveedor/addProveedor'; ?>">
          <section class="wrapper_l_padd p_b_60">  
            <h2 class="subtittles azul"><i class="material-icons">place</i> Información General</h2>    
            <section class="panel_all padd_all m_b_30">
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="bg_user_p relative inline cargar_img">
                  <a href="#" class="file-select"><i class="material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar imagen">photo_camera</i></a>
                    <div class="user_profile" style="width: 250px;height: 250px;">
                      <img src="<?= base_url() ?>assets/images/user.jpg" alt="">
                    </div> 
                  <input id="file-preview" name="file" type="file" class="file2 hidden"/>
                </div>
              </fieldset>          
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="name" type="text" class="validate" name="nombre_text">
                  <label for="name">NOMBRE DEL PROVEEDOR</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>              
              </fieldset>
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="email" type="email" name="correo_text" class="validate">
                  <label for="email">CORREO ELECTRÓNICO*</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  <span class="error" style="display:none;">Debes ser un correo valido.</span>
                </div>              
              </fieldset>
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="nit" type="number" name="NIT_number" class="validate">
                  <label for="nit">NIT</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>
              </fieldset>
              <!--fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="contact" type="text" name="persona_contacto_text" class="validate">
                  <label for="contact">PERSONA DE CONTACTO</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>              
              </fieldset-->
            </section>

            <section class="row m_b_30">
              <section class="large-6 medium-6 small-12 columns">
                <h2 class="subtittles azul"><i class="material-icons">place</i> Información Sedes</h2><br>
                <div class="panel_all padd_all panel_add2">              
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" id="searchTextField" type="text" placeholder="INGRESA AQUÍ LA DIRECCIÓN*" name="direccionSede" onchange="codeAddress()">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <div class="bgmap map_new">
                      <div id="map"></div>
                    </div>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" type="hidden" placeholder="Latitud" name="Latitude">
                    <input class="validate" type="hidden" placeholder="Longitud" name="Longitude">
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" type="text" placeholder="PERSONA DE CONTACTO *" name="personContactSede">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" type="email" placeholder="CORREO *" name="correoSede">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    <span class="error" style="display:none;">Debes ser un correo valido.</span>
                  </fieldset>
                  <fieldset class="input-field large-6 medium-6 small-12 columns padding newphone">                  
                    <a href="javascript:void(0)"><i class="material-icons">add_circle_outline</i></a>
                    <input class="validate" type="number" placeholder="TELÉFONOS*" name="tel1Sede">  
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                
                  </fieldset>
                  <fieldset class="input-field large-6 medium-6 small-12 columns padding more_phones">                  
                    <input class="validate" type="number" placeholder="TELÉFONOS 2" name="tel2Sede">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field large-6 medium-6 small-12 columns padding">                  
                    <input class="validate" type="text" placeholder="CIUDAD*" name="ciudadSede">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" type="text" placeholder="NOMBRE DE LA SEDE*" name="nameSede">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <div class="input-field select_custon">
                      <select class="validateSelect" multiple name="diasSede">
                        <option value="" disabled selected>DÍAS DE ATENCIÓN</option>
                        <option value="Lunes">Lunes</option>
                        <option value="Martes">Martes</option>
                        <option value="Miercoles">Miércoles</option>
                        <option value="Jueves">Jueves</option>
                        <option value="Viernes">Viernes</option>
                        <!--option value="Sabado">Sabado</option-->
                      </select>
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </div>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <span>Horario de atención Lunes a Viernes</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding"> 
                      <div>       
                        <select class="validateSelect" name="desdeHora">
                          <option value="" disabled selected>Desde</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>

                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">        
                      <div>
                        <select class="validateSelect" name="hastaHora">
                          <option value="" disabled selected>Hasta</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <span>Horario de atención día Sabado</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding"> 
                      <div>       
                        <select class="validateSelect" name="desdeHoraSabado">
                          <option value="" selected>Desde</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>

                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">        
                      <div>
                        <select class="validateSelect" name="hastaHoraSabado">
                          <option value="" selected>Hasta</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>
                  </fieldset>
                    <div class="clear"></div>
                  <a class="clic_more padd_v sin_margin wow fadeInUp" id="addSede">
                    <img src="<?= base_url() ?>assets/images/more.png" alt="">
                    <h2>AGREGAR SEDE</h2>
                  </a>
                  <a class="clic_more padd_v sin_margin wow fadeInUp" id="actualizarSede" style="display: none;">
                    <img src="<?= base_url() ?>assets/images/more.png" alt="">
                    <h2>ACTUALIZAR SEDE</h2>
                  </a>
                </div>
              </section>
              <section class="large-6 medium-6 small-12 columns">
                <h2 class="subtittles azul"><i class="material-icons">place</i> Sedes Creadas</h2><label style="color: #a2228e;">La primera sede creada será la principal</label>
                <div class="panel_all padd_all panel_add2 box">
                  <div class="padd_all">
                    <table class="bordered table_new_prov">
                      <!-- <thead>
                        <tr>
                            <th data-field="id">Name</th>
                            <th data-field="name">Item Name</th>
                            <th data-field="price">Item Price</th>
                        </tr>
                      </thead> -->
                      <tbody id="contentSede">
                      </tbody>
                    </table>
                  </div>
                  <div class="msj" id="notSede">No se ha creado ninguna sede</div>
                </div>
              </section>
            </section>

            <section class="row m_b_30">
              <section class="large-6 medium-6 small-12 columns">
                <h2 class="subtittles azul"><i class="material-icons">check_box</i> Servicios</h2>
                <div class="panel_all padd_all panel_add">
                  <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">                  
                    <select class="validateSelect" name="categoria">
                      <option value="" disabled selected>Selecciona una categoría</option>
                      <?php foreach ($categorias as $value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->nombre_text; ?></option>
                      <?php } ?>

                    </select>
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">                  
                    <select class="validateSelect" name="servicio">
                      <option value="" disabled selected>Selecciona un servicio</option>
                      <?php foreach ($servicios as $value) { ?>
                        <option value="<?php echo $value->id; ?>"><?php echo $value->name_text; ?></option>
                      <?php } ?>
                    </select>
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                  <span>Ingresa el rango de precio</span>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <fieldset class="input-field large-6 medium-6 small-12 columns padding">
                      <input type="text" class="validate" placeholder="Desde" name="desdePrice">
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </fieldset>
                    <fieldset class="input-field large-6 medium-6 small-12 columns padding">
                      <input type="text" class="validate" placeholder="Hasta" name="hastaPrice">
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </fieldset>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                    <input class="validate" type="text" placeholder="NOMBRE DEL SERVICIO *" name="nameService">
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </fieldset>
                    <div class="clear"></div>
                  <a class="clic_more padd_v sin_margin wow fadeInUp" id="addService">
                    <img src="<?= base_url() ?>assets/images/more.png" alt="">
                    <h2>AGREGAR SERVICIO</h2>
                  </a>
                  <a class="clic_more padd_v sin_margin wow fadeInUp" id="actualizarService" style="display: none;">
                    <img src="<?= base_url() ?>assets/images/more.png" alt="">
                    <h2>ACTUALIZAR SERVICIO</h2>
                  </a>
                </div>
              </section>
              <section class="large-6 medium-6 small-12 columns">
                <h2 class="subtittles azul"><i class="material-icons">check_box</i> Servicios Creados</h2>
                <div class="panel_all padd_all panel_add box">
                  <div class="padd_all">
                    <table class="bordered table_new_prov">
                      <!-- <thead>
                        <tr>
                            <th data-field="id">Name</th>
                            <th data-field="name">Item Name</th>
                            <th data-field="price">Item Price</th>
                        </tr>
                      </thead> -->
                      <tbody id="contentServices"></tbody>
                    </table>
                  </div>
                  <div class="msj" id="notService">No se ha creado ningún servicio</div>
                </div>
              </section>
            </section>

            <a id="btnAddProveedor" class="clic_more p_t_30 wow fadeIn">
              <img src="<?= base_url() ?>assets/images/more.png" alt="">
              <h2>AGREGAR PROVEEDOR</h2>
              <span class="msgErrorProveedor"></span>
            </a>


          </section>
        </form>

      </section>    

      <?php echo $footer; ?>     

    </main>

    <!--script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvvMFVyH-v7FRoTnHwaUmOOgJKA6Wcmj4&libraries=places"></script>
    <script type="text/javascript">
      var locations = [
        ['sede', 4.6745991904396496, -74.08683452124023, 4]
      ];

      var image = '../assets/images/market.png';
      geocoder = new google.maps.Geocoder();
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        scrollwheel: false,
        icon: image,
        center: new google.maps.LatLng(4.6745991904396496, -74.08683452124023),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          icon: image,
          map: map,
          draggable: true
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

        var latitude = $("#addProveedorForm input[name=Latitude]");
        var longitude = $("#addProveedorForm input[name=Longitude]");

        latitude.val(4.6745991904396496);
        longitude.val(-74.08683452124023);

        marker.addListener('drag', function() {
          var markerLatLng = marker.getPosition();
          var markerLatitude = markerLatLng.lat();
          var markerLongitude = markerLatLng.lng();
          latitude.val(markerLatitude);
          longitude.val(markerLongitude);
        });

        var defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(4.6745991904396496, -74.08683452124023)
        );

        var input = document.getElementById('searchTextField');
        var options = {
          bounds: defaultBounds,
          types: ['address'],
          //types: ['(cities)'],
          componentRestrictions: {country: 'co'},
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);

      }

      function codeAddress() {
        console.log(".");
        var address = document.getElementById("searchTextField").value;
        geocoder.geocode( { 'address': address}, function(results, status) {
          console.log(results);
          marker.setPosition(results[0].geometry.location);
          map.setCenter(results[0].geometry.location);
          var latitude = $("#addProveedorForm input[name=Latitude]");
          var longitude = $("#addProveedorForm input[name=Longitude]");
          var markerLatLng = marker.getPosition();
          var markerLatitude = markerLatLng.lat();
          var markerLongitude = markerLatLng.lng();
          latitude.val(markerLatitude);
          longitude.val(markerLongitude);
        });
      }


      //form validate
      $( ".validate" ).change(function() {
          var father = $(this).parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $(".validateSelect").change(function() {
          var father = $(this).parent().parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $("#addProveedorForm input[name=correo_text]").change(function() {
          var email = $("#addProveedorForm input[name=correo_text]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#register-form input[name=correo_text]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });

      $("#addProveedorForm input[name=correoSede]").change(function() {
          var email = $("#addProveedorForm input[name=correoSede]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#register-form input[name=correoSede]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });
    </script>

  </body>
</html>
