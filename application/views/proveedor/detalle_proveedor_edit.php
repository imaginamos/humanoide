    <?php echo $header; ?>
    <div id="base_url" style="display:none;"><?php echo(base_url()); ?></div>
    <a id="btnUpdateProveedor" class="btn-floating btn-large waves-effect waves-light tooltipped" data-position="bottom" data-delay="50" data-tooltip="Guardar" style="position: fixed; top: 80%; left: 90%"><i class="material-icons">save</i></a>
    <section class="intert_h animated fadeInUp">
        <div class="tittle">
          <div class="wrapper_l relative">
            <a href="<?= base_url().'proveedor'; ?>" class="back"><i class="material-icons">arrow_back</i> Volver al Listado</a>
            <div class="inline">              
              <h1>Formulario para editar proveedor</h1>
            </div>
          </div>
        </div>
        <form id="updateProveedorForm" action="<?php echo base_url().'proveedor/updateProveedor'; ?>">
          <input type="hidden" name="idProveedor" value="<?php echo $proveedor->id; ?>">
          <section class="wrapper_l_padd p_b_60">  
            <h2 class="subtittles azul"><i class="material-icons">place</i> Información General</h2>    
            <section class="panel_all padd_all m_b_30">
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="bg_user_p relative inline cargar_img">
                  <a href="#" class="file-select"><i class="material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar imagen">photo_camera</i></a>
                    <div class="user_profile" style="width: 250px;height: 250px;">
                    <?php if(!empty($proveedor->imagen_file)){ ?>
                    	<img src="<?php echo base_url().'uploads/files/'.$proveedor->imagen_file; ?>" alt="">
                    <?php }else{ ?>
                      	<img src="<?= base_url() ?>assets/images/icon_empresa.jpg" alt="">
                    <?php } ?>
                    </div> 
                  <input id="file-preview" name="file" type="file" class="file2 hidden"/>
                </div>
              </fieldset>          
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="name" type="text" class="validate" name="nombre_text" value="<?php echo $proveedor->nombre_text; ?>">
                  <label for="name">NOMBRE DEL PROVEEDOR</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>              
              </fieldset>
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="email" type="email" name="correo_text" class="validate" value="<?php echo $proveedor->correo_text; ?>">
                  <label for="email">CORREO ELECTRÓNICO*</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  <span class="error" style="display:none;">Debes ser un correo valido.</span>
                </div>              
              </fieldset>
              <fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="nit" type="number" name="NIT_number" class="validate" value="<?php echo $proveedor->NIT_number; ?>">
                  <label for="nit">NIT</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>
              </fieldset>
              <!--fieldset class="large-6 medium-6 small-12 columns padd_all">
                <div class="input-field">
                  <input id="contact" type="text" name="persona_contacto_text" class="validate">
                  <label for="contact">PERSONA DE CONTACTO</label>
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>              
              </fieldset-->
            </section>
            
            <?php $count = 0; ?>
            <?php foreach ($sedes as $sede) { ?>
            <input type="hidden" id="idSede<?php echo($count); ?>" value="<?php echo $sede[0]->id; ?>">
            <section class="row m_b_30 sedeItem">
              <section class="large-12 medium-12 small-12 columns">
                <h2 class="subtittles azul"><i class="material-icons">place</i> Información Sedes</h2>
		            <div class="panel_all padd_all panel_add2">
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
		                <input class="validate" type="text" placeholder="INGRESA AQUÍ LA DIRECCIÓN*" id="direccionSede<?php echo($count); ?>" value="<?php echo $sede[0]->direccion_text; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
		                <div class="bgmap map_new">
		                  <div id="map<?php echo($count); ?>" style="width: 100%; height: 500px;"></div>
		                </div>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">
		              	<?php $map = explode(",", $sede[0]->pos_map); ?>              
		                <input id="latitud<?php echo($count); ?>" class="validate" type="text" placeholder="Latitud" value="<?php echo($map[0]); ?>">
		                <input id="longitud<?php echo($count); ?>" class="validate" type="text" placeholder="Longitud" value="<?php echo($map[1]); ?>">
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
		                <input class="validate" type="text" placeholder="PERSONA DE CONTACTO *" id="personContactSede<?php echo($count); ?>" value="<?php echo $sede[0]->persona_contacto_text; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
		                <input class="validate" type="email" placeholder="CORREO *" id="correoSede<?php echo($count); ?>" value="<?php echo $sede[0]->correo_electronico_text; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		                <span class="error" style="display:none;">Debes ser un correo valido.</span>
		              </fieldset>
		              <fieldset class="input-field large-6 medium-6 small-12 columns padding newphone">                  
		                <a href="javascript:void(0)"><i class="material-icons">add_circle_outline</i></a>
		                <input class="validate" type="number" placeholder="TELÉFONOS*" id="tel1Sede<?php echo($count); ?>" value="<?php echo $sede[0]->telefono_1_number; ?>">  
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                
		              </fieldset>
		              <fieldset class="input-field large-6 medium-6 small-12 columns padding more_phones">                  
		                <input class="validate" type="number" placeholder="TELÉFONOS 2" id="tel2Sede<?php echo($count); ?>" value="<?php echo $sede[0]->telefono_2_number; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		              </fieldset>
		              <fieldset class="input-field large-6 medium-6 small-12 columns padding">                  
		                <input class="validate" type="text" placeholder="CIUDAD*" id="ciudadSede<?php echo($count); ?>" value="<?php echo $sede[0]->ciudad_text; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">
		                <input id="name<?php echo($count); ?>" class="validate" type="text" placeholder="NOMBRE DE LA SEDE*" value="<?php echo $sede[0]->nombre_text; ?>">
		                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">            
		                <div class="input-field select_custon">
		                  <select class="validateSelect" multiple id="diasSede<?php echo($count); ?>">
                        <option value="" disabled >DÍAS DE ATENCIÓN</option>
                        <?php if (stripos($sede[0]->dias_de_atencion_multiselect,"unes") ) { ?>
                          <option value="Lunes" selected>Lunes</option>
                        <?php }else{ ?>
                          <option value="Lunes">Lunes</option>
                        <?php } ?>

                        <?php if (stripos($sede[0]->dias_de_atencion_multiselect,"artes") ) { ?>
                          <option value="Martes" selected>Martes</option>
                        <?php }else{ ?>
                          <option value="Martes">Martes</option>
                        <?php } ?>

                        <?php if (stripos($sede[0]->dias_de_atencion_multiselect,"iercoles") ) { ?>
                          <option value="Miercoles" selected>Miercoles</option>
                        <?php }else{ ?>
                          <option value="Miercoles">Miercoles</option>
                        <?php } ?>

                        <?php if (stripos($sede[0]->dias_de_atencion_multiselect,"ueves") ) { ?>
                          <option value="Jueves" selected>Jueves</option>
                        <?php }else{ ?>
                          <option value="Jueves">Jueves</option>
                        <?php } ?>

                        <?php if (stripos($sede[0]->dias_de_atencion_multiselect,"iernes") ) { ?>
                          <option value="Viernes" selected>Viernes</option>
                        <?php }else{ ?>
                          <option value="Viernes">Viernes</option>
                        <?php } ?>
		                  </select>
		                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		                </div>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">
		              	<span>Horario de atención Lunes - Viernes</span>
		              </fieldset>
		              <fieldset class="input-field large-12 medium-12 small-12 columns padding">
		                <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding"> 
		                  <div>       
		                    <select class="validateSelect" id="desdeHora<?php echo($count); ?>" value="<?php echo $sede[0]->horario_de_atencion_inicio_text; ?>">
		                      <option value="" disabled>Desde</option>
		                      <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_inicio_text == ($i.':00 AM') ) { ?>
                              <option value="<?php echo($i.':00 AM'); ?>" selected ><?php echo($i.':00 AM'); ?></option>
                            <?php }else{ ?>
  		                        <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                            <?php } ?>
		                      <?php } ?>
		                      <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_inicio_text == ($i.':00 PM') ) { ?>
                              <option value="<?php echo($i.':00 PM'); ?>" selected ><?php echo($i.':00 PM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                            <?php } ?>
		                        
		                      <?php } ?>
		                    </select>
		                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		                  </div>
		                </fieldset>

		                <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">        
		                  <div>
		                    <select class="validateSelect" id="hastaHora<?php echo($count); ?>">
		                      <option value="" disabled>Hasta</option>
		                      <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_fin_text == ($i.':00 AM') ) { ?>
                              <option value="<?php echo($i.':00 AM'); ?>" selected ><?php echo($i.':00 AM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                            <?php } ?>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_fin_text == ($i.':00 PM') ) { ?>
                              <option value="<?php echo($i.':00 PM'); ?>" selected ><?php echo($i.':00 PM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                            <?php } ?>
                            
                          <?php } ?>
		                    </select>
		                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
		                  </div>
		                </fieldset>
		              </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <span>Horario de atención Sabado</span>
                  </fieldset>
                  <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding"> 
                      <div>       
                        <select class="validateSelect" id="desdeHoraSabado<?php echo($count); ?>">
                          <option value="">Desde</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_sabado_inicio_text == ($i.':00 AM') ) { ?>
                              <option value="<?php echo($i.':00 AM'); ?>" selected ><?php echo($i.':00 AM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                            <?php } ?>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_sabado_inicio_text == ($i.':00 PM') ) { ?>
                              <option value="<?php echo($i.':00 PM'); ?>" selected ><?php echo($i.':00 PM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                            <?php } ?>
                            
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>

                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">        
                      <div>
                        <select class="validateSelect" id="hastaHoraSabado<?php echo($count); ?>">
                          <option value="">Hasta</option>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_sabado_fin_text == ($i.':00 AM') ) { ?>
                              <option value="<?php echo($i.':00 AM'); ?>" selected ><?php echo($i.':00 AM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 AM'); ?>" ><?php echo($i.':00 AM'); ?></option>
                            <?php } ?>
                          <?php } ?>
                          <?php for ($i=1; $i <= 12 ; $i++) { ?>
                            <?php if ($sede[0]->horario_de_atencion_sabado_fin_text == ($i.':00 PM') ) { ?>
                              <option value="<?php echo($i.':00 PM'); ?>" selected ><?php echo($i.':00 PM'); ?></option>
                            <?php }else{ ?>
                              <option value="<?php echo($i.':00 PM'); ?>" ><?php echo($i.':00 PM'); ?></option>
                            <?php } ?>
                            
                          <?php } ?>
                        </select>
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </div>
                    </fieldset>
                  </fieldset>
		            </div>
                </section>
            </section>
		        <?php $count = $count + 1; ?>
            <?php } ?>
              
            
            <section class="row m_b_30">
            <section class="large-12 medium-12 small-12 columns">
                  <h2 class="subtittles azul"><i class="material-icons">check_box</i> Servicios</h2>
            <?php $countService = 0; ?>
            <?php foreach ($serviciosProveedor as $servicio) { ?>
                  <input type="hidden" id="idService<?php echo($countService); ?>"  value="<?php echo $servicio->id; ?>">
                  <div class="panel_all padd_all panel_add seviceItem">
                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">    
                      <select class="validateSelect" id="categoria<?php echo($countService); ?>">
                        <option value="" disabled>Selecciona una categoría</option>
                        <?php foreach ($categorias as $value) { ?>
                          <?php if ($servicio->proveedor_categorias_relation == $value->id) { ?>
                            <option value="<?php echo $value->id; ?>" selected><?php echo $value->nombre_text; ?></option>
                          <?php }else{ ?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->nombre_text; ?></option>
                          <?php } ?>
                          
                        <?php } ?>

                      </select>
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </fieldset>
                    <fieldset class="input-field select_custon large-6 medium-6 small-12 columns padding">                  
                      <select class="validateSelect" id="servicio<?php echo($countService); ?>">
                        <option value="" disabled>Selecciona un servicio</option>
                        <?php foreach ($serviciosHumanoide as $value) { ?>
                          <?php if ($servicio->servicios_humanoide_relation == $value->id) { ?>
                            <option value="<?php echo $value->id; ?>" selected><?php echo $value->name_text; ?></option>
                          <?php }else{ ?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->name_text; ?></option>
                          <?php } ?>
                        <?php } ?>
                      </select>
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </fieldset>
                    <span>Ingresa el rango de precio</span>
                    <fieldset class="input-field large-12 medium-12 small-12 columns padding">
                      <fieldset class="input-field large-6 medium-6 small-12 columns padding">
                        <input type="text" class="validate" placeholder="Desde" id="desdePrice<?php echo($countService); ?>" value="<?php echo $servicio->precio_desde_number; ?>" >
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </fieldset>
                      <fieldset class="input-field large-6 medium-6 small-12 columns padding">
                        <input type="text" class="validate" placeholder="Hasta" id="hastaPrice<?php echo($countService); ?>" value="<?php echo $servicio->precio_hasta_number; ?>">
                        <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                      </fieldset>
                    </fieldset>
                    <fieldset class="input-field large-12 medium-12 small-12 columns padding">                  
                      <input class="validate" type="text" placeholder="NOMBRE DEL SERVICIO *" id="nameService<?php echo($countService); ?>" value="<?php echo $servicio->nombre_text; ?>">
                      <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    </fieldset>
                      <div class="clear"></div>
                  </div>
                  <?php $countService = $countService + 1; ?>
            <?php } ?>
              </section>
            </section>
          

          </section>   
        </form>

      </section>
        

      <?php echo $footer; ?>     

    </main>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">

    for (var m = 0; m < $(".sedeItem").length; m++) {
     

      var latitud = $("#latitud"+m)[0].value;
      var longitud = $("#longitud"+m)[0].value;
      var name = $("#name"+m)[0].value;

      locations = [];

      var locations = [
        [name, parseFloat(latitud), parseFloat(longitud), 4]
      ];

      var image = '../assets/images/market.png';

      var map = new google.maps.Map(document.getElementById('map'+m), {
        zoom: 13,
        scrollwheel: false,
        icon: image,
        center: new google.maps.LatLng(parseFloat(latitud), parseFloat(longitud)),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          icon: image,
          map: map,
          draggable: true
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

        var latitude = $("#latitud"+m);
        var longitude = $("#longitud"+m);

        marker.addListener('drag', function() {
          var markerLatLng = marker.getPosition();
          var markerLatitude = markerLatLng.lat();
          var markerLongitude = markerLatLng.lng();
          latitude.val(markerLatitude);
          longitude.val(markerLongitude);
        });

      }
    }


      //form validate
      $( ".validate" ).change(function() {
          var father = $(this).parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $(".validateSelect").change(function() {
          var father = $(this).parent().parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $("#addProveedorForm input[name=correo_text]").change(function() {
          var email = $("#addProveedorForm input[name=correo_text]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#register-form input[name=correo_text]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });

      $("#addProveedorForm input[name=correoSede]").change(function() {
          var email = $("#addProveedorForm input[name=correoSede]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#register-form input[name=correoSede]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });

      

    </script>

  </body>
</html>
