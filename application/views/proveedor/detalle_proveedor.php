    <?php echo $header; ?>
    <a href="<?= base_url().'proveedor/viewDetalleProveedor?proveedor='.$_GET["proveedor"].'&mode=edit' ?>" class="btn-floating btn-large waves-effect waves-light tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar la información" style="position: fixed; top: 80%; left: 90%"><i class="material-icons">mode_edit</i></a>
    <div id="idProveedor" style="display: none;"><?= $_GET["proveedor"]; ?></div>
      <section class="intert_h animated fadeInUp">
        <div class="tittle">
          <div class="wrapper_l relative">
            <a href="<?= base_url().'proveedor'; ?>" class="back"><i class="material-icons">arrow_back</i> Volver al Listado</a>
            <h3 class="cod">COD: <?php echo $proveedor->codigo_humanoide_number; ?></h3>
              <?php if(!empty($proveedor->imagen_file)){ ?>
                <div class="icon_emp" style="background-image: url('<?php echo base_url().'uploads/files/'.$proveedor->imagen_file; ?>');"></div> 
               <?php }else{ ?>
                <div class="icon_emp" style="background-image: url('<?php echo base_url(); ?>assets/images/icon_empresa.png');"></div> 
               <?php } ?>
            <div class="inline">              
              <h1><?php echo $proveedor->nombre_text; ?></h1>
              <p>Nit: <?php echo $proveedor->NIT_number; ?></p>
            </div>
          </div>
        </div>
        <section class="wrapper_l_padd p_b_60">  
          <h2 class="subtittles"><i class="material-icons">place</i> Sedes <a href="javascript:go_to('.list_services')">Ir a Servicios</a></h2>    
          <section class="list_sedes">
            <?php $count = 0; ?>
            <?php foreach ($sedes as $sede) { ?>
              <article class="wow fadeInUp sedeItem">
                <section class="panel_all">
                  <div class="small-12 medium-6 large-6 columns padd_all">
                    <section class="small-12 medium-12 large-6 columns padding bg_list">
                      <h3 class="sub_list"><i class="material-icons">group</i> Información General</h3>
                      <ul class="list_gen">
                        <li><p id="name<?php echo($count); ?>"><?php echo $sede[0]->nombre_text; ?></p></li>
                        <li><p><?php echo $sede[0]->persona_contacto_text; ?></p></li>
                        <li><p><?php echo $sede[0]->correo_electronico_text; ?></p></li>
                      </ul>
                    </section>
                    <section class="small-12 medium-12 large-6 columns padding bg_list">
                      <h3 class="sub_list"><i class="material-icons">place</i> Ubicación</h3>
                      <ul class="list_gen">
                        <li><p><?php echo $sede[0]->ciudad_text; ?></p></li>
                        <li><p><?php echo $sede[0]->direccion_text; ?></p></li>
                      </ul>
                    </section>
                      <div class="clear"></div>
                    <section class="small-12 medium-12 large-6 columns padding bg_list">
                      <h3 class="sub_list"><i class="material-icons">group</i> Números de contacto</h3>
                      <ul class="list_gen">
                        <li><p><?php echo $sede[0]->telefono_1_number; ?></p></li>
                        <li><p><?php echo $sede[0]->telefono_2_number; ?></p></li>
                      </ul>
                    </section>
                    <section class="small-12 medium-12 large-6 columns padding bg_list">
                      <h3 class="sub_list"><i class="material-icons">place</i> Horario de atención</h3>
                      <ul class="list_gen">
                        <li><?php echo str_replace(",", ", ", $sede[0]->dias_de_atencion_multiselect); ?><p>De <?php echo $sede[0]->horario_de_atencion_inicio_text; ?> A <?php echo $sede[0]->horario_de_atencion_fin_text; ?> </p></li>
                        <?php if (!empty($sede[0]->horario_de_atencion_sabado_inicio_text) && !empty($sede[0]->horario_de_atencion_sabado_fin_text)) { ?>
                          <li>Sabado <br><p>De <?php echo $sede[0]->horario_de_atencion_sabado_inicio_text; ?> A <?php echo $sede[0]->horario_de_atencion_sabado_fin_text; ?> </p></li>
                        <?php } ?>
                      </ul>
                    </section>
                  </div> 
                  <?php $map = explode(",", $sede[0]->pos_map); ?>
                  <div id="latitud<?php echo($count); ?>" style="display: none;"><?php echo($map[0]); ?></div>
                  <div id="longitud<?php echo($count); ?>" style="display: none;"><?php echo($map[1]); ?></div>
                  
                  <div class="small-12 medium-6 large-6 columns">
                      <div class="bgmap">
                        <div id="map<?php echo($count); ?>" style="width: 100%; height: 500px;"></div>
                      </div>
                  </div>   
                </section>
              </article>
              <?php $count = $count + 1; ?>
            <?php } ?>

          <h2 class="subtittles"><i class="material-icons">place</i> Servicios</h2>
          <section class="list_services">
            <section class="panel_all">
              <article class="padd_all">
              <?php foreach ($categorias as $value) { ?>
                <section class="small-12 medium-6 large-3 columns padding bg_list">
                  <h3 class="sub_list"><i class="material-icons">check_box</i> <?php echo $value["categoria"][0]->nombre_text; ?></h3>
                  <ul class="list_gen">
                  <?php foreach ($value["servicios"][0] as $servicio) { ?>
                    <li><p><?php echo $servicio->nombre_text; ?> <br>Desde: $<?php echo $servicio->precio_desde_number; ?></p></li>
                  <?php } ?>
                  </ul>
                </section>
              <?php } ?>
              </article>   
            </section>    
          </section>

          <a id="btnAddProveedorDestacado" class="clic_more p_t_30 wow fadeIn">
            <img src="<?= base_url() ?>assets/images/more.png" alt="">
            <h2>AGREGAR PROVEEDOR</h2>
            <span id="msgErrorProveedorDestacado"></span>
          </a>

        </section>
        
      </section>    

      <?php echo $footer; ?>     

    </main>

    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">

    for (var m = 0; m < $(".sedeItem").length; m++) {
      var latitud = $("#latitud"+m)[0].innerHTML;
      var longitud = $("#longitud"+m)[0].innerHTML;
      var name = $("#name"+m)[0].innerHTML;

      locations = [];

      var locations = [
        [name, parseFloat(latitud), parseFloat(longitud), 4]
      ];

      var image = '../assets/images/market.png';

      var map = new google.maps.Map(document.getElementById('map'+m), {
        zoom: 13,
        scrollwheel: false,
        icon: image,
        center: new google.maps.LatLng(parseFloat(latitud), parseFloat(longitud)),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          icon: image,
          map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

      }
    }

      

    </script>

  </body>
</html>
