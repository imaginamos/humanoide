      <?php echo $header; ?>

      <!-- Inicio Slider -->
      <section class="bg_slider">
        <ul class="carrusel_slider">  
          <li class="item">
            <a href="">
              <img src="<?= base_url() ?>assets/images/bg_faq.png" alt=""/>
              <div class="cont_text_slider">                  
                <h1>Preguntas frecuentes y Tutoriales Humanoide</h1>
              </div>
              <div class="mask"></div>
            </a>
          </li>
        </ul>        
      </section>
      <!--Fin Slider-->

        <section class="wrapper_l">
          <div class="padding">
            <h2 class="subtittles azul border"><i class="material-icons">message</i> <?php echo $faqTitle; ?> <a class="right clic_more_prod">Ver más Preguntas</a></h2>
            <div class="search icon">
              <i class="material-icons">search</i>
              <?php if (!empty($_GET["searchFAQ"])) { ?>
                <input type="text" placeholder="Buscar" id="searchFAQ" value="<?php echo $_GET["searchFAQ"]; ?>">
              <?php }else{ ?>
                <input type="text" placeholder="Buscar" id="searchFAQ">
              <?php } ?>
            </div>       
          </div>  
          <?php $count = 0; ?>          
          <ul class="features_h">
          <?php for ($i=0; $i < count($faq); $i++) { ?>
            <?php if($count < 6): ?>
               <li class="small-12 medium-6 large-4 columns padding">                
                <div class="bg_panel sin_overf">           
                    <div class="preg">
                      <h1><?php echo $faq[$i]->title; ?></h1>
                    </div>
                    <div class="txt padd_all txt_faq">
                      <p><?php echo $faq[$i]->content; ?></p>
                    </div>
                </div>
              </li>
              <?php $count = $count + 1; ?>
            <?php endif ?>   
          <?php  } ?>
          </ul>  
          <ul class="features_h animated fadeInUp more_products">
          <?php for ($i=6; $i < count($faq); $i++) { ?>
            <?php if($count >= 6): ?>
               <li class="small-12 medium-6 large-4 columns padding">                
                <div class="bg_panel sin_overf">           
                    <div class="preg">
                      <h1><?php echo $faq[$i]->title; ?></h1>
                    </div>
                    <div class="txt padd_all txt_faq">
                      <p><?php echo $faq[$i]->content; ?></p>
                    </div>
                </div>
              </li>
              <?php $count = $count + 1; ?>
            <?php endif ?>
            
          <?php  } ?>
          </ul>    

          <div class="clear"></div>
        </section>
        
        <section class="wrapper_l p_b_60">
          <div class="padding">
            <h2 class="subtittles azul border"><i class="material-icons">play_circle_outline</i> <?php echo $videoTitle; ?> <a href="javascript:void(0)" class="right clic_more_videos">Ver más Tutoriales</a></h2>                   
          </div>            
          <?php $countVideos = 0; ?>
          <ul class="features_h wow fadeInUp">
          <?php for ($i=0; $i < count($videos); $i++) { ?>
            <?php if($countVideos < 6): ?>
              <?php 
              $video1 = str_replace("<p>", "", $videos[$i]->content);
              $video = str_replace("</p>", "", $video1); ?>
               <li class="small-12 medium-6 large-4 columns padding">                
                <div class="bg_panel">
                  <a href='<?php echo $video; ?>' class="modalVideo" data-effect="mfp-zoom-out">
                    <div class="bg_video">
                      <div class="mask"></div>
                      <img src="<?= base_url() ?>assets/images/video1.png" alt="">
                    </div>
                    <div class="txt padd_all">
                      <h2><?php echo $videos[$i]->title; ?></h2>
                    </div>
                  </a>
                </div>
              </li>
              <?php $countVideos = $countVideos + 1; ?>
            <?php endif ?>   
          <?php  } ?>
          </ul>
          <ul class="features_h animated fadeInUp more_videos">
          <?php for ($i=6; $i < count($videos); $i++) { ?>
            <?php if($countVideos >= 6): ?>
              <?php 
              $video1 = str_replace("<p>", "", $videos[$i]->content);
              $video = str_replace("</p>", "", $video1); ?>
               <li class="small-12 medium-6 large-4 columns padding">                
                <div class="bg_panel">
                  <a href='<?php echo $video; ?>' class="modalVideo" data-effect="mfp-zoom-out">
                    <div class="bg_video">
                      <div class="mask"></div>
                      <img src="<?= base_url() ?>assets/images/video1.png" alt="">
                    </div>
                    <div class="txt padd_all">
                      <h2><?php echo $videos[$i]->title; ?></h2>
                    </div>
                  </a>
                </div>
              </li>
              <?php $countVideos = $countVideos + 1; ?>
            <?php endif ?>   
          <?php  } ?>
          </ul>

          <div class="clear"></div>
        </section>

      </section>
    

      <?php echo $footer; ?>     

    </main>

  </body>
</html>
