    <?php echo $header; ?>
      <section class="intert_h animated fadeInUp">

        <!--a href="<?= base_url().'Usuario/viewEditPerfil' ?>" class="btn-floating btn-large waves-effect waves-light btn_floating_profile tooltipped" data-position="left" data-delay="50" data-tooltip="Editar la información"><i class="material-icons">mode_edit</i></a-->
        
        <section class="bg_portada_user">
          <section class="wrapper_l relative">
            <div class="bg_user_p relative inline cargar_img">
              
              <?php if(!empty($user->imagen_perfil_file)){ ?>
                <div class="user_profile">            
                  <img src="<?= base_url().'uploads/perfil/'.$user->imagen_perfil_file; ?>" alt="">
                </div> 
               <?php }else{ ?>
                <div class="user_profile">            
                  <img src="<?= base_url() ?>assets/images/user.jpg" alt="">
                </div> 
               <?php } ?>
              
              <!--input id="file-preview" name="file" type="file" class="file2 hidden" /-->
            </div>
            <div class="date_user inline">
              <h2 class="subtittles"><?php echo $user->nombre_o_razon_social_text; ?></h2>
              <p><?php echo $user->email_text;  ?></p>
              <p><?php echo $user->password_text;?></p>
              <a href="#edit_pass" class="back modal_clic">Cambiar Contraseña</a>   
            </div> 
             <a href="<?= base_url().'Usuario/viewEditPerfil' ?>" class="btn-floating btn-large waves-effect waves-light tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar la información" style="position: fixed; top: 80%;"><i class="material-icons">mode_edit</i></a>
           </section>
        </section>        
        
        <section class="wrapper_l p_b_60">  
          <section class="small-12 medium-12 large-12 columns padd_all date_profile">              
            <h3 class="sub_list"><i class="material-icons">group</i> Información General</h3>
            <div class="panel_all padd_all edit_dates_user">
              <fieldset class="large-3 medium-6 small-12 columns padd_all">
                <div class="block" style="margin-top: 30px;">
                  <label>TIPO DE PERSONA</label>
                  <h3><?php echo$user->tipo_persona_select; ?></h3>
                </div>   
                <div class="block" style="margin-top: 30px;">
                  <label>TIPO DE DOCUMENTO</label>
                  <h3><?php echo $user->tipo_documento_select; ?></h3>
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>NÚMERO DE DOCUMENTO</label>
                  <h3 name="numero_documento_number" type="number"><?php echo $user->numero_documento_number; ?></h3>
                  </div>
                <div class="block" style="margin-top: 30px;">
                  <label>EMPRESA</label>
                  <h3  name="empresa_text"><?php echo $user->empresa_text; ?></h3>                   
                </div> 
                <div class="block" style="margin-top: 30px;">
                  <label>DIRECCIÓN</label>
                  <h3 name="direccion_text" ><?php echo $user->direccion_text; ?></h3>                   
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>CIUDAD</label>
                  <h3 name="ciudad_text" ><?php echo $user->ciudad_text; ?></h3>                   
                </div>  
                <div class="block" style="margin-top: 30px;">
                  <label>DEPARTAMENTO</label>
                  <h3  name="departamento_text"><?php echo $user->departamento_text; ?></h3>                   
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>PAÍS</label>
                  <h3  name="pais_text"><?php echo $user->pais_text; ?></h3>                   
                </div>
              </fieldset>
              <fieldset class="large-3 medium-6 small-12 columns padd_all"> 
                <div class="block" style="margin-top: 30px;">
                  <label>WEBSITE</label>
                  <h3  name="website_text"><?php echo $user->website_text; ?></h3>                   
                </div>   
                <div class="block" style="margin-top: 30px;">
                  <label>TELÉFONO</label>
                  <h3 type="number" name="telefono_number"><?php echo $user->telefono_number; ?></h3>                  
                </div> 
                <div class="block" style="margin-top: 30px;">
                  <label>CELULAR</label>
                  <h3 type="number" name="celular_number"><?php echo $user->celular_number; ?></h3>      
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>FAX</label>
                  <h3  name="fax_number"><?php echo $user->fax_number; ?></h3>                   
                </div>   
                <div class="block" style="margin-top: 30px;">
                  <label>NOMBRE PERSONA DE CONTACTO</label>
                  <h3  name="nombre_persona_contacto_text"><?php echo $user->nombre_persona_contacto_text; ?></h3></div>                     
                <div class="block" style="margin-top: 30px;">
                  <label>CARGO</label>
                  <h3  name="cargo_text"><?php echo $user->cargo_text; ?></h3>                  
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>NOMBRE REPRESENTANTE LEGAL</label>
                  <h3  name="nombre_representante_legal_text"><?php echo $user->nombre_representante_legal_text; ?></h3>                 
                </div>    
                <div class="block" style="margin-top: 30px;">
                  <label>TIPO DE DOCUMENTO</label>
                  <div class="input-field select_custon">
                  <h3><?php echo $user->tipo_documento_representante_select; ?></h3>
                  </div>
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>NÚMERO DE DOCUMENTO</label>
                  <h3 type="number" name="numero_documento_representante_legal_number"><?php echo $user->numero_documento_representante_legal_number; ?></h3>
                </div>                                                       
              </fieldset>
              <fieldset class="large-3 medium-6 small-12 columns padd_all">
                <div class="block" style="margin-top: 30px;">
                  <label>Información de facturación</label>                   
                  <h3  name="informacion_de_facturacion_text"><?php echo $user->informacion_de_facturacion_text; ?></h3>
                </div>
                <div class="block" style="margin-top: 30px;">
                  <label>Autorretenedor</label>
                  <h3  name="autorretenedor_text"><?php echo $user->autorretenedor_text; ?></h3>
                </div>                  
                <div class="block" style="margin-top: 30px;">
                  <label>Numero de resolución</label>
                  <h3 type="number" name="numero_de_resolucion_number"><?php echo $user->numero_de_resolucion_number; ?></h3>
                </div>     
                <div class="block" style="margin-top: 30px;">
                  <label>Fecha</label>
                  <h3 type="date" name="fecha_text"><?php echo $user->fecha_text; ?></h3>                   
                </div>     
                <div class="block" style="margin-top: 30px;">
                  <label>Régimen IVA</label>
                  <h3  name="regimen_IVA_text"><?php echo $user->regimen_IVA_text; ?></h3>                 
                </div>     
                <div class="block" style="margin-top: 30px;">
                  <label>Tipo de contribuyente</label>
                  <h3  name="tipo_de_contribuyente_text"><?php echo $user->tipo_de_contribuyente_text; ?></h3>
                </div>    
                <div class="block" style="margin-top: 30px;">
                  <label>Tipo de régimen</label>
                  <h3  name="tipo_de_regimen_text"><?php echo $user->tipo_de_regimen_text; ?></h3>
                </div>             
              </fieldset>
              <fieldset class="large-3 medium-6 small-12 columns padd_all"> 
                <div class="block" style="margin-top: 30px;">
                  <label>Actividad económica</label>
                  <h3  name="actividad_economica_text"><?php echo $user->actividad_economica_text; ?></h3>
                </div> 
                <div class="block" style="margin-top: 30px;">
                  <label>Tarifa</label>
                  <h3  name="tarifa_number"><?php echo $user->tarifa_number; ?></h3>                 
                </div> 
                <div class="block" style="margin-top: 30px;">
                  <label>Código CIIU</label>
                  <h3  name="codigo_CIIU"><?php echo $user->codigo_CIIU; ?></h3>                 
                </div>       
                <div class="block" style="margin-top: 30px;">
                  <label>Descripción actividad</label>
                  <h3 name="descripcion_actividad_text" ><?php echo $user->descripcion_actividad_text; ?></h3>
                </div>                                        
              </fieldset>
            </div> 
          </section>

            <div class="clear"></div>

          <h3 class="sub_list p_t_20"><i class="material-icons">group</i> Información del Plan Adquirido</h3>
          <section class="panel_all m_b_30">          
            <section class="small-12 medium-12 large-12 columns padd_all">            
              <ul class="row txt_datos list_planes">
                <li class="small-12 medium-4 large-4 columns padding">
                  <div class="bg_plan">
                      <div class="title">
                        <h3>Plan Gratis</h3>
                      </div>
                      <h4 class="price">$0</h4>
                      <div class="descuento">
                        <p><b>Precio Anual</b></p>
                      </div>
                      <div class="price_menu">
                        <h2>$0</h2>
                        <p>Precio Mensual</p>
                      </div> 
                      <div class="description">
                        <p><span class="block">Dos perfiles</span>Tres Proveedores por categoría</p>
                        <p>Vence: 03/10/2016</p>
                      </div>
                      <div class="centerBtn">
                        <a href="#" class="btnb cir border">Cambiar</a>
                      </div>
                    </div>
                </li>
                <li class="small-12 medium-4 large-4 columns padding activo">
                  <div class="bg_plan">
                      <div class="title">
                        <h3>Plan Básico</h3>
                      </div>
                      <h4 class="price">$900.000</h4>
                      <div class="descuento">
                        <p><b>Precio Anual</b> Ahorro del 20%</p>
                      </div>
                      <div class="price_menu">
                        <h2>$130.000</h2>
                        <p>Precio Mensual</p>
                      </div> 
                      <div class="description">
                        <p><span class="block">Ocho perfiles</span> Proveedores Ilimitados</p>
                        <p>Vence: 03/10/2016</p>
                      </div>
                      <div class="centerBtn">
                        <a href="#" class="btnb cir border">ACTUAL</a>
                      </div>
                    </div>
                </li>
                <li class="small-12 medium-4 large-4 columns padding">
                  <div class="bg_plan">
                      <div class="title">
                        <h3>Plan Premium</h3>
                      </div>
                      <h4 class="price">$1’200.000</h4>
                      <div class="descuento">
                        <p><b>Precio Anual</b> Ahorro del 20%</p>
                      </div>
                      <div class="price_menu">
                        <h2>$300.000</h2>
                        <p>Precio Mensual</p>
                      </div> 
                      <div class="description">
                        <p><span class="block">Perfiles Ilimitados</span> Proveedores Ilimitados</p>
                        <p>Vence: 03/10/2016</p>
                      </div>
                      <div class="centerBtn">
                        <a href="#" class="btnb cir border">cambiar</a>
                      </div>
                    </div>
                </li>
              </ul>
            </section>
          </section>

        </section>

      </section>    

      <?php echo $footer; ?>    

    </main>
    
  </body>
</html>
