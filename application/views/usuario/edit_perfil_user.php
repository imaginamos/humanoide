    <?php echo $header; ?>
    <form id="formPerfilUser" action="<?= base_url().'Usuario/editPerfil' ?>">
      <section class="intert_h animated fadeInUp">

        <!--a class="btn-floating btn-large waves-effect waves-light btn_floating_profile tooltipped savePerfilUser" data-position="left" data-delay="50" data-tooltip="Guardar"><i class="material-icons">done</i></a-->
        
        <section class="bg_portada_user">
          <section class="wrapper_l">
            <div class="bg_user_p relative inline cargar_img">
              <a href="#" class="file-select"><i class="material-icons tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar imagen">photo_camera</i></a>
              <?php if(!empty($user->imagen_perfil_file)){ ?>
                <div class="user_profile">            
                  <img src="<?= base_url().'uploads/perfil/'.$user->imagen_perfil_file; ?>" alt="">
                </div> 
               <?php }else{ ?>
                <div class="user_profile">            
                  <img src="<?= base_url() ?>assets/images/user.jpg" alt="">
                </div> 
               <?php } ?>
              <input id="file-preview" name="file" type="file" class="file2 hidden"/>
            </div>
            <div class="date_user inline">
              <h2 class="subtittles">
                <input class="validate" name="nombre_o_razon_social_text" type="text" value="<?php echo $user->nombre_o_razon_social_text; ?>">
                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
              </h2>
              
              <p>
                <input class="validate" type="text" name="email_text" value="<?php echo $user->email_text;  ?>">
                <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                <span class="error" style="display:none;">Debes ser un correo valido.</span>
              </p>
              <p><?php echo $user->password_text;?></p>
              <a href="#edit_pass" class="back modal_clic">Cambiar Contraseña</a>     
            </div> 
             <a class="btn-floating btn-large waves-effect waves-light tooltipped savePerfilUser" data-position="bottom" data-delay="50" data-tooltip="Guardar" style="position: fixed; top: 80%;"><i class="material-icons">done</i></a>
           </section>
        </section>         
        
        <section class="wrapper_l_padd p_b_60">  
          <section class="small-12 medium-12 large-12 columns padd_all date_profile">              
            <h3 class="sub_list"><i class="material-icons">group</i> Información General</h3>
            <div class="panel_all padd_all edit_dates_user">
              <fieldset class="large-3 medium-6 small-12 columns padd_all">
                <div class="block">
                  <label>TIPO DE PERSONA</label>
                  <div class="input-field select_custon">
                    <select class="validateSelect" name="tipo_persona_select" id="">
                    <?php if(empty($user->tipo_persona_select)){ ?>
                      <option value="" disabled selected>TIPO DE PERSONA</option>
                      <option value="Juridica">Jurídica</option>
                      <option value="Natural">Natural</option>
                    <?php }else{ ?>
                      <option value="" disabled>TIPO DE PERSONA</option>
                      <?php if ($user->tipo_persona_select == "Juridica") { ?>
                        <option value="Juridica" selected>Jurídica</option>
                        <option value="Natural">Natural</option>
                      <?php }else{ ?>
                        <option value="Juridica">Jurídica</option>
                        <option value="Natural" selected>Natural</option>
                      <?php } ?> 
                    <?php } ?>
                    </select>
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                    
                  </div>
                </div>   
                <div class="block">
                  <label>TIPO DE DOCUMENTO</label>
                  <div class="input-field select_custon">
                    <select class="validateSelect" name="tipo_documento_select">
                      <?php if(empty($user->tipo_documento_select)){ ?>
                        <option value="" disabled selected>TIPO DE DOCUMENTO</option>
                        <option value="NIT">Nit</option>
                        <option value="cedula">Cedula</option>
                        <option value="cedula extranjera">Cedula de extranjería</option>
                      <?php }else{ ?>
                        <option value="" disabled>TIPO DE DOCUMENTO</option>
                        <?php if ($user->tipo_documento_select == "NIT") { ?>
                          <option value="NIT" selected>Nit</option>
                          <option value="cedula">Cedula</option>
                          <option value="cedula extranjera">Cedula de extranjería</option>
                        <?php }else if($user->tipo_documento_select == "cedula"){ ?>
                          <option value="NIT">Nit</option>
                          <option value="cedula" selected>Cedula</option>
                          <option value="cedula extranjera">Cedula de extranjería</option>
                        <?php }else{ ?>
                          <option value="NIT">Nit</option>
                          <option value="cedula" >Cedula</option>
                          <option value="cedula extranjera" selected>Cedula de extranjería</option>
                        <?php } ?> 
                      <?php } ?>
                    </select>
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </div>
                </div>
                <div class="block">
                  <label>NÚMERO DE DOCUMENTO</label>
                  <input class="validate" name="numero_documento_number" type="number" value="<?php echo $user->numero_documento_number; ?>">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>
                <div class="block">
                  <label>EMPRESA</label>
                  <input class="validate" type="text" value="<?php echo $user->empresa_text; ?>" name="empresa_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div> 
                <div class="block">
                  <label>DIRECCIÓN</label>
                  <input class="validate" name="direccion_text" type="text" value="<?php echo $user->direccion_text; ?>">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>
                <div class="block">
                  <label>CIUDAD</label>
                  <input class="validate" name="ciudad_text" type="text" value="<?php echo $user->ciudad_text; ?>">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>  
                <div class="block">
                  <label>DEPARTAMENTO</label>
                  <input class="validate" type="text" value="<?php echo $user->departamento_text; ?>" name="departamento_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>
                <div class="block">
                  <label>PAÍS</label>
                  <input class="validate" type="text" value="<?php echo $user->pais_text; ?>" name="pais_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>          
                <!--div class="block">
                  <label>CORREO ELECTRÓNICO</label>
                  <input class="validate" type="email" value="ejemplo@miempresa.com">                    
                </div-->                                  
              </fieldset>
              <fieldset class="large-3 medium-6 small-12 columns padd_all"> 
                <div class="block">
                  <label>WEBSITE</label>
                  <input class="validate" type="text" value="<?php echo $user->website_text; ?>" name="website_text"> 
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>   
                <div class="block">
                  <label>TELÉFONO</label>
                  <input class="validate" type="number" value="<?php echo $user->telefono_number; ?>" name="telefono_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div> 
                <div class="block">
                  <label>CELULAR</label>
                  <input class="validate" type="number" value="<?php echo $user->celular_number; ?>" name="celular_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>       
                </div>
                <div class="block">
                  <label>FAX</label>
                  <input class="validate" type="text" value="<?php echo $user->fax_number; ?>" name="fax_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>   
                <div class="block">
                  <label>NOMBRE PERSONA DE CONTACTO</label>
                  <input class="validate" type="text" value="<?php echo $user->nombre_persona_contacto_text; ?>" name="nombre_persona_contacto_text"> 
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>                     
                <div class="block">
                  <label>CARGO</label>
                  <input class="validate" type="text" value="<?php echo $user->cargo_text; ?>" name="cargo_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>
                <div class="block">
                  <label>NOMBRE REPRESENTANTE LEGAL</label>
                  <input class="validate" type="text" value="<?php echo $user->nombre_representante_legal_text; ?>" name="nombre_representante_legal_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                  
                </div>    
                <div class="block">
                  <label>TIPO DE DOCUMENTO</label>
                  <div class="input-field select_custon">
                    <select class="validateSelect" name="tipo_documento_representante_select">
                      <?php if(empty($user->tipo_documento_representante_select)){ ?>
                        <option value="" disabled selected>TIPO DE DOCUMENTO</option>
                        <option value="NIT">Nit</option>
                        <option value="cedula">Cedula</option>
                        <option value="cedula extranjera">Cedula de extranjería</option>
                      <?php }else{ ?>
                        <option value="" disabled>TIPO DE DOCUMENTO</option>
                        <?php if ($user->tipo_documento_representante_select == "NIT") { ?>
                          <option value="NIT" selected>Nit</option>
                          <option value="cedula">Cedula</option>
                          <option value="cedula extranjera">Cedula de extranjería</option>
                        <?php }else if($user->tipo_documento_representante_select == "cedula"){ ?>
                          <option value="NIT">Nit</option>
                          <option value="cedula" selected>Cedula</option>
                          <option value="cedula extranjera">Cedula de extranjería</option>
                        <?php }else{ ?>
                          <option value="NIT">Nit</option>
                          <option value="cedula" >Cedula</option>
                          <option value="cedula extranjera" selected>Cedula de extranjería</option>
                        <?php } ?> 
                      <?php } ?>
                    </select>
                    <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                  </div>
                </div>
                <div class="block">
                  <label>NÚMERO DE DOCUMENTO</label>
                  <input class="validate" type="number" value="<?php echo $user->numero_documento_representante_legal_number; ?>" name="numero_documento_representante_legal_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>                                                       
              </fieldset>

              <fieldset class="large-3 medium-6 small-12 columns padd_all">
                <div class="block">
                  <label>Información de facturación</label>                   
                  <input class="validate" type="text" value="<?php echo $user->informacion_de_facturacion_text; ?>" name="informacion_de_facturacion_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>
                </div>
                <div class="block">
                  <label>Autorretenedor</label>
                  <input class="validate" type="text" value="<?php echo $user->autorretenedor_text; ?>" name="autorretenedor_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>                  
                <div class="block">
                  <label>Numero de resolución</label>
                  <input class="validate" type="number" value="<?php echo $user->numero_de_resolucion_number; ?>" name="numero_de_resolucion_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>     
                <div class="block">
                  <label>Fecha</label>
                  <input class="validate" type="date" value="<?php echo $user->fecha_text; ?>" name="fecha_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>     
                <div class="block">
                  <label>Régimen IVA</label>
                  <input class="validate" type="text" value="<?php echo $user->regimen_IVA_text; ?>" name="regimen_IVA_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                   
                </div>     
                <div class="block">
                  <label>Tipo de contribuyente</label>
                  <input class="validate" type="text" value="<?php echo $user->tipo_de_contribuyente_text; ?>" name="tipo_de_contribuyente_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>    
                <div class="block">
                  <label>Tipo de régimen</label>
                  <input class="validate" type="text" value="<?php echo $user->tipo_de_regimen_text; ?>" name="tipo_de_regimen_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                    
                </div>             
              </fieldset>
              <fieldset class="large-3 medium-6 small-12 columns padd_all">
                <!--div class="block">
                  <label>Numero de resolución</label>
                  <input class="validate" type="number" value="<?php echo $user->numero_documento_number; ?>">

                </div>     
                <div class="block">
                  <label>Fecha</label>
                  <input class="validate" type="date" value="<?php echo $user->numero_documento_number; ?>">
                </div-->   
                <div class="block">
                  <label>Actividad económica</label>
                  <input class="validate" type="text" value="<?php echo $user->actividad_economica_text; ?>" name="actividad_economica_text">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>      
                </div>   
                <!--div class="block">
                  <label>CIUDAD</label>
                  <input class="validate" type="text" value="Bogotá">                      
                </div-->    
                <div class="block">
                  <label>Tarifa</label>
                  <input class="validate" type="text" value="<?php echo $user->tarifa_number; ?>" name="tarifa_number">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                  
                </div> 
                <div class="block">
                  <label>Código CIIU</label>
                  <input class="validate" type="text" value="<?php echo $user->codigo_CIIU; ?>" name="codigo_CIIU">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span>                  
                </div>       
                <div class="block">
                  <label>Descripción actividad</label>
                  <!-- <textarea name="" id="" cols="30" rows="10" class="materialize-textarea"></textarea>      -->
                  <input class="validate" name="descripcion_actividad_text" type="text" value="<?php echo $user->descripcion_actividad_text; ?>">
                  <span class="empty" style="display:none;">Debes llenar este campo continuar.</span> 
                </div>                                        
              </fieldset>
            </div> 
          </section>
          <!-- <section class="small-12 medium-12 large-6 columns padding bg_list date_profile">
            <h3 class="sub_list"><i class="material-icons">credit_card</i> Información de Facturación</h3>
            <div class="panel_all padd_all edit_dates_user">
              <section class="padd_all">
                <fieldset class="large-6 medium-6 small-12 columns padd_all">
                  <div class="block">
                    <label>NÚMERO DE  DOCUMENTO</label>
                    <input class="validate" type="number" value="102340545003">                      
                  </div>
                  <div class="block">
                    <label>DIRECCIÓN</label>
                    <input class="validate" type="text" value="Carrera 19 # 90-10">                      
                  </div>  
                  <div class="block">
                    <label>CIUDAD</label>
                    <input class="validate" type="text" value="Bogotá">                      
                  </div>           
                  <div class="block">
                    <label>DEPARTAMENTO</label>
                    <input class="validate" type="text" value="Bogotá D.C.">                      
                  </div>     
                  <div class="block">
                    <label>PAÍS</label>
                    <input class="validate" type="text" value="Colombia">                      
                  </div>   
                  <div class="block">
                    <label>Actividad económica</label>
                    <input class="validate" type="text" value="<?php echo $user->numero_documento_number; ?>">                    
                  </div>    
                  <div class="block">
                    <label>Actividad económica</label>
                    <input class="validate" type="text" value="">                    
                  </div>   
                  <div class="block">
                    <label>Tarifa</label>
                    <input class="validate" type="text" value="">                    
                  </div>                                              
                </fieldset>
                <fieldset class="large-6 medium-6 small-12 columns padd_all">
                  <div class="block">
                    <label>Autorretenedor</label>
                    <input class="validate" type="text" value="">                    
                  </div>                  
                  <div class="block">
                    <label>Numero de resolución</label>
                    <input class="validate" type="number" value="">                    
                  </div>     
                  <div class="block">
                    <label>Fecha</label>
                    <input class="validate" type="date" value="">                    
                  </div>     
                  <div class="block">
                    <label>Régimen IVA</label>
                    <input class="validate" type="text" value="">                    
                  </div>     
                  <div class="block">
                    <label>Tipo de contribuyente</label>
                    <input class="validate" type="text" value="">                    
                  </div>    
                  <div class="block">
                    <label>Tipo de régimen</label>
                    <input class="validate" type="text" value="">                    
                  </div>                                   
                  <div class="block">
                    <label>Código CIIU</label>
                    <input class="validate" type="text" value="">                    
                  </div>             
                  <div class="block">
                    <label>Descripción actividad</label>
                    <textarea name="" id="" cols="30" rows="10" class="materialize-textarea"></textarea>     
                    <input class="validate" type="text">
                  </div>                         
                </fieldset>
              </section>
            </div>
          </section> -->

            <div class="clear"></div>

          <section class="padd_all">
            <h3 class="sub_list p_t_20"><i class="material-icons">group</i> Información del Plan Adquirido</h3>
            <section class="panel_all m_b_30">          
              <section class="small-12 medium-12 large-12 columns padd_all">            
                <ul class="row txt_datos list_planes">
                  <li class="small-12 medium-4 large-4 columns padding activo">
                    <div class="bg_plan">
                        <div class="title">
                          <h3>Plan Gratis</h3>
                        </div>
                        <h4 class="price">$0</h4>
                        <div class="descuento">
                          <p><b>Precio Anual</b></p>
                        </div>
                        <div class="price_menu">
                          <h2>$0</h2>
                          <p>Precio Mensual</p>
                        </div> 
                        <div class="description">
                          <p><span class="block">Dos perfiles</span>Tres Proveedores por categoría</p>
                          <p>Vence: 03/10/2016</p>
                        </div>
                        <div class="centerBtn">
                          <a href="#" class="btnb cir border">Actual</a>
                        </div>
                      </div>
                  </li>
                  <li class="small-12 medium-4 large-4 columns padding">
                    <div class="bg_plan">
                        <div class="title">
                          <h3>Plan Básico</h3>
                        </div>
                        <h4 class="price">$900.000</h4>
                        <div class="descuento">
                          <p><b>Precio Anual</b> Ahorro del 20%</p>
                        </div>
                        <div class="price_menu">
                          <h2>$130.000</h2>
                          <p>Precio Mensual</p>
                        </div> 
                        <div class="description">
                          <p><span class="block">Ocho perfiles</span> Proveedores Ilimitados</p>
                          <p>Vence: 03/10/2016</p>
                        </div>
                        <div class="centerBtn">
                          <a href="#" class="btnb cir border">cambiar</a>
                        </div>
                      </div>
                  </li>
                  <li class="small-12 medium-4 large-4 columns padding">
                    <div class="bg_plan">
                        <div class="title">
                          <h3>Plan Premium</h3>
                        </div>
                        <h4 class="price">$1’200.000</h4>
                        <div class="descuento">
                          <p><b>Precio Anual</b> Ahorro del 20%</p>
                        </div>
                        <div class="price_menu">
                          <h2>$300.000</h2>
                          <p>Precio Mensual</p>
                        </div> 
                        <div class="description">
                          <p><span class="block">Perfiles Ilimitados</span> Proveedores Ilimitados</p>
                          <p>Vence: 03/10/2016</p>
                        </div>
                        <div class="centerBtn">
                          <a href="#" class="btnb cir border">cambiar</a>
                        </div>
                      </div>
                  </li>
                </ul>
              </section>
            </section>
          </section>

        </section>

      </form>
      </section>
      <?php echo $footer; ?>   

    </main>

    <script type="text/javascript">
        //form validate
      $(".validate").change(function() {
          var father = $(this).parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $(".validateSelect").change(function() {
          var father = $(this).parent().parent();
          if (this == "") {
              father.find(".empty").fadeIn( "slow" );
              father.find(".empty").attr('style', 'display: block !important;');
          }else{
              father.find(".empty").fadeOut( "slow" );
          }
      });

      $("#formPerilUser input[name=email_text]").change(function() {
          var email = $("#register-form input[name=email_text]").val();
          var atpos = email.indexOf("@");
          var dotpos = email.lastIndexOf(".");
          var father = $(this).parent();
          if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
            father.find(".error").fadeIn( "slow" );
            father.find(".error").attr('style', 'display: block !important; color: #a2228e;');
            $("#register-form input[name=email_text]").focus();
          }else{
            father.find(".error").fadeOut( "slow" );
          }
      });
      </script>

  </body>
</html>
