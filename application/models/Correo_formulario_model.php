<?php
/**
 * Description of Correo_formulario_model
 *
 * @author DAVID SANCHEZ
 */
class Correo_formulario_model extends CI_Model{
    
    function get_CorreoToForm($form){
        $this->db->like('formulario_multiselect', (String)$form);
        $query = $this->db->get("correos_formulario");
        return $query->result();
    }
    
    
}
