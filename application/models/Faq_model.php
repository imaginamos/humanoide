<?php

/**
 * Description of Faq_model
 *
 * @author DAVID SANCHEZ
 */
class Faq_model extends CI_Model {
    
    function getFaqQuestionToSearch($search) {
        $this->db->select('preguntas_steps');
        $result = $this->db->get("faq_preguntas")->result();
        $question = json_decode($result[0]->preguntas_steps);
        
        $arrayFaq = [];
        foreach ($question as $value) {
            if(stripos($value->title, $search)){
                array_push($arrayFaq, $value);
            }else if(stripos($value->content, $search)){
                array_push($arrayFaq, $value);
            }
        }
        return $arrayFaq;
    }
}
