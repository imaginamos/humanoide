<?php

/**
 * Description of Proveedor_model
 *
 * @author DAVID SANCHEZ
 */
class Proveedor_model extends CI_Model {
    
    function getProveedorById($id){
        $this->db->where('id', (int)$id);
        return $this->db->get("proveedor")->result();
    }
    
    function gerProveedorDestacados(){
        $this->db->where('destacado_checkbox', "Destacar");
        return  $this->db->get("proveedor")->result();
    }
            
    function getCiudadesProveedores() {
        $this->db->select('ciudad_text');
        $this->db->distinct(true);
        return  $this->db->get("proveedor_sedes")->result();
    }
    
    function getProveedoresToCiudad($city) {
        $this->db->where('ciudad_text', $city);
        $arraySedes = $this->db->get("proveedor_sedes")->result();
        $proveedores = $this->db->get("proveedor")->result();
        
        $arrayProveedores = [];
        foreach ($proveedores as $proveedor) {
            $sedesProveedor = explode(",", $proveedor->proveedor_sedes_multirelation);
            foreach ($sedesProveedor as $sedeProveedor) {
                foreach ($arraySedes as $sede) {
                    if($sede->id == $sedeProveedor ){
                        array_push($arrayProveedores, $proveedor);
                        break 2;
                    }
                }
            } 
        }
        return $arrayProveedores;      
    }
    
    function getProveedoresToName($name) {
        $this->db->like('nombre_text', $name);
        return $this->db->get("proveedor")->result();
    }
    
    function getCategoriasProveedores() {
        return $this->db->get("proveedor_categorias")->result();
    }
    
    function getServiciosHumanoide() {
        return $this->db->get("servicios_humanoide")->result();
    }
    
    function getCategoriasNoDestacado() {
        $categorias = $this->db->get("proveedor_categorias")->result();
        $arrayCategorias = [];
        foreach ($categorias as $value) {
            if($value->nombre_text != "Proveedores destacados"){
                array_push($arrayCategorias, $value);
            }
        }
        return $arrayCategorias;
    } 
    
    function getServiciosProveedor($id) {
        $this->db->where('id', (int)$id);
        $proveedor = $this->db->get("proveedor")->result();
        
        $servicios = explode(",", $proveedor[0]->proveedor_servicios_multirelation);
        $infoServicios = [];
        foreach ($servicios as $value) {
            //get service
            $this->db->where('id', (int)$value);
            $servicio = $this->db->get("proveedor_servicios")->result();
            array_push($infoServicios, $servicio[0]);
        }
        return $infoServicios;
    }
    
    function updateProveedor($id,$data) {
        $this->db->where('id', (int)$id);
        return $this->db->update('proveedor',$data);
    }
    
    function updateService($id,$data) {
        $this->db->where('id', (int)$id);
        return $this->db->update('proveedor_servicios',$data);
    }
    
    function updateSede($id,$data) {
        $this->db->where('id', (int)$id);
        return $this->db->update('proveedor_sedes',$data);
    }
    
}
