    <?php 
class Humanoide extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
            $data = [];
            if(empty($_SESSION["customer"])){
                $tables = $this->db->get("administrable_table")->result();
                foreach ($tables as $table) {
                    $data["tables"][$table->name] = $this->db->get($table->name)->result();
                }
                
                $titleBanner = $data["tables"]["banner"][0]->titulo_imagen_text;
                $arrayTitle = [];
                for ($i = 0; $i < strlen($titleBanner); $i+=30) {
                    array_push($arrayTitle, substr($titleBanner,$i, 30));
                }
                $data["title"] = implode("<br>", $arrayTitle);
                
                $subtitleBanner = $data["tables"]["banner"][0]->subtitulo_imagen_text;
                $arraySubtitle = [];
                for ($i = 0; $i < strlen($subtitleBanner); $i+=60) {
                    array_push($arraySubtitle, substr($subtitleBanner,$i, 60));
                }
                $data["subtitle"] = implode("<br>", $arraySubtitle);
               
                $this->load->view('/humanoide/index', $data);
            }  else {
                $this->db->where('id', (int)$_SESSION["customer"]);
                $customer = $this->db->get("usuarios")->result();
                $data["user"] = $customer[0];
                
                $this->load->model('Proveedor_model');
            
                //proveedores destacados
                $data["destacados"] = $this->Proveedor_model->gerProveedorDestacados();
                $data["ciudades"] = $this->Proveedor_model->getCiudadesProveedores();
                
                //categorias
                $data["categorias"] = $this->db->get("proveedor_categorias")->result();

                if(!empty($_GET["categoria"])){
                    $this->db->where('usuarios_relation', (int)$_SESSION["customer"]);
                    $proveedor = $this->db->get("proveedor")->result();

                    $serviciosFinal = [];
                    foreach ($proveedor as $value) {
                        $status = false;
                        $serviciosProveedor = explode(",", $value->proveedor_servicios_multirelation);
                        foreach ($serviciosProveedor as $servicio) {
                            $this->db->where('id', $servicio);
                            $s = $this->db->get("proveedor_servicios")->result();  
                            if($s[0]->proveedor_categoria_relation == $_GET["categoria"]){
                                $status = true;
                            }
                        }   
                        if($status == true){
                            array_push($serviciosFinal, $value); 
                        }

                    }
                    $data["proveedor"] = $serviciosFinal;
                }else{
                    //prooveedores
                    $this->db->like('usuarios_multirelation', (int)$_SESSION["customer"]);
                    $data["proveedor"] = $this->db->get("proveedor")->result();
                }
                
                $data["tab"] = "proveedor";
                $data["header"] = $this->load->view('/common/header', $data, true);
                $data["footer"] = $this->load->view('/common/footer', $data, true);
                $this->load->view('/proveedor/index', $data);
            }
            
	}
        
        function createContact(){
            
            if ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            $data["created_at"] = date('Y-m-d h:i:s');
            $this->db->insert("solicitud_contacto",$data);
            
            //Sanitize input data using PHP filter_var().
	    $name = filter_var($data["nombre_text"], FILTER_SANITIZE_STRING);
            $email = filter_var($data["email_text"], FILTER_SANITIZE_EMAIL);
            $comentario = filter_var($data["mensaje_textarea"], FILTER_SANITIZE_STRING);
                    	    
	    $mensaje = '<br /> Solicitud de contacto <br />'.
                    '<br /> Nombre: ' . $name .
                    '<br /> Correo electronico: ' . $email .
                    '<br /> Comentario: ' . $comentario;
            
            $this->load->library('enviar_correo');
            $this->load->model('Correo_formulario_model');            
            
            $mails = $this->Correo_formulario_model->get_CorreoToForm("Contactenos");
            $this->enviar_correo->sendEmail($mensaje, $mails, "Contactenos");
            }
	}
        
        function createProveedor(){
            
            if ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            $arrayCiudadDireccion = explode(" ", $data["direccion_text"]);
            $data["ciudad_text"] = $arrayCiudadDireccion[0];
            $data["direccion_text"] = $arrayCiudadDireccion[2];
            $data["created_at"] = date('Y-m-d h:i:s');
            $this->db->insert("candidatos_proveedores",$data);
	       
	    //Sanitize input data using PHP filter_var().
	    $nameEmpresa = filter_var($data["nombre_empresa_text"], FILTER_SANITIZE_STRING);
            $tipoDcto = filter_var($data["tipo_documento_select"], FILTER_SANITIZE_STRING);
            $numDcto = filter_var($data["numero_documento_number"], FILTER_SANITIZE_NUMBER_INT);
            $nameRepresent = filter_var($data["nombre_persona_contacto_text"], FILTER_SANITIZE_STRING);
            $cargo = filter_var($data["cargo_persona_contacto_text"], FILTER_SANITIZE_STRING);
            $email = filter_var($data["email_text"], FILTER_SANITIZE_EMAIL);
            $telefono = filter_var($data["telefono_number"], FILTER_SANITIZE_NUMBER_INT);
            $ciudad = filter_var($data["ciudad_text"], FILTER_SANITIZE_STRING);
            $direccion = filter_var($data["direccion_text"], FILTER_SANITIZE_STRING);
            $servicios = filter_var($data["servicios_humanoide_relation"], FILTER_SANITIZE_STRING);
            $comentario = filter_var($data["comentario_textarea"], FILTER_SANITIZE_STRING);
                    	    
	    $mensaje = '<br /> Solicitud de proveedor <br />'.
                    '<br /> Nombre de la empresa: ' . $nameEmpresa .
                    '<br /> Tipo de documento: ' . $tipoDcto .
                    '<br /> Número de documento: ' . $numDcto .
                    '<br /> Nombre del representante: ' . $nameRepresent .
                    '<br /> Cargo: ' . $cargo .
                    '<br /> Correo electronico: ' . $email .
                    '<br /> Telefono: ' . $telefono .
                    '<br /> Ciudad: ' . $ciudad .
                    '<br /> Dirección: ' . $direccion .
                    '<br /> Servicio: ' . $servicios .
                    '<br /> Comentario: ' . $comentario;
            
            $this->load->library('enviar_correo');
            $this->load->model('Correo_formulario_model');            
            
            $mails = $this->Correo_formulario_model->get_CorreoToForm("Solicitud proveedor");
            $this->enviar_correo->sendEmail($mensaje, $mails, "Solicitud de proveedor");	    
            }
	}
        
        function registro(){
            
            if ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            
                if ($this->db->where('email_text',$data["email_text"])->get("usuarios")->result()) {
                    $array["status"] = 'error';
                    $array["msg"] = 'Lo sentimos, este correo ya ha sido registrado anteriormente.';
                    echo json_encode(array ($array));
                }else{
                    $data["created_at"] = date('Y-m-d h:i:s');
                    
                    if($data["g-recaptcha-response"]){
                        unset($data["password_confirm"],$data["g-recaptcha-response"],$data["terminosCondiciones"],$data["politicaTratamientos"]);
                        $data["terminos_y_condiciones_checkbox"] = "Si";
                        $data["politicas_y_tratamiento_de_datos_checkbox"] = "Si";
                        $this->db->insert("usuarios",$data);

                        //Sanitize input data using PHP filter_var().
                        $name = filter_var($data["nombre_o_razon_social_text"], FILTER_SANITIZE_STRING);
                        $email = filter_var($data["email_text"], FILTER_SANITIZE_EMAIL);
                        $telefono = filter_var($data["telefono_number"], FILTER_SANITIZE_NUMBER_INT);
                        $pass = filter_var($data["password_text"], FILTER_SANITIZE_STRING);

                        $mensaje = '<br /> Registro de usuario <br />'.
                            '<br /> Nombre de la empresa: ' . $name .
                            '<br /> Correo electronico: ' . $email .
                            '<br /> Telefono: ' . $telefono .
                            '<br /> Su contraseña es: ' . $pass;

                        $this->load->library('enviar_correo');
                        $this->load->model('Correo_formulario_model');            

                        $mails = $this->Correo_formulario_model->get_CorreoToForm("Registro");
                        $this->enviar_correo->sendEmail($mensaje, $mails, "Usuario registrado");
                        
                        $user = $this->db->where('email_text',$data["email_text"])->get("usuarios")->result();
                        $_SESSION["customer"] = $user[0]->id;
                        
                        $array["status"] = 'success';
                        $array["msg"] = 'Tu registro ha sido exitoso.';
                        echo json_encode(array ($array));
                    }else{
                        $array["status"] = 'error';
                        $array["msg"] = 'Debes aceptar el Captcha'; 
                        echo json_encode(array ($array));
                    } 
                }
            }
	}
        
        function login(){
            
            if ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            if ($this->db->where('email_text',$data["email_text"])->get("usuarios")->result()) {
                $user = $this->db->where('email_text',$data["email_text"])->get("usuarios")->result();
                if ($user[0]->password_text == $data["password_text"] ) {
                    $this->session->data['order_id'];
                    $array["status"] = 'success';
                    $array["msg"] = 'Has iniciado sesión.';
                    if(empty($_SESSION["customer"])){
                        $_SESSION["customer"] = $user[0]->id;
                    }
                    $array["href"] = base_url()."";
                    echo json_encode(array ($array));
                }  else {
                    $array["status"] = 'error';
                    $array["msg"] = 'Contraseña incorrecta.';
                    echo json_encode(array ($array)); 
                }
            }else{
                $array["status"] = 'error';
                $array["msg"] = 'Lo sentimos, este correo no esta registrado.';
                echo json_encode(array ($array));
            }
            
            }
	}
}
?> 