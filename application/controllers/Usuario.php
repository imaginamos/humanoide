<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author USUARIO
 */
class Usuario extends CI_Controller{
    //put your code here
    function __construct(){
        parent::__construct();
    }
    
    function index(){
        $data = [];
        if(empty($_SESSION["customer"])){
            $tables = $this->db->get("administrable_table")->result();
            foreach ($tables as $table) {
                $data["tables"][$table->name] = $this->db->get($table->name)->result();
            }
            $this->load->view('/humanoide/index', $data);
        }  else {
            $this->db->where('id', (int)$_SESSION["customer"]);
            $customer = $this->db->get("usuarios")->result();
            $data["user"] = $customer[0];
            
            $data["header"] = $this->load->view('/common/header', $data, true);
            $data["footer"] = $this->load->view('/common/footer', $data, true);
            $this->load->view('/usuario/perfil_user', $data);
        }
    }
    
    function viewEditPerfil() {
        $data = [];
        if(empty($_SESSION["customer"])){
            $tables = $this->db->get("administrable_table")->result();
            foreach ($tables as $table) {
                $data["tables"][$table->name] = $this->db->get($table->name)->result();
            }
            $this->load->view('/humanoide/index', $data);
        }  else {
            $this->db->where('id', (int)$_SESSION["customer"]);
            $customer = $this->db->get("usuarios")->result();
            $data["user"] = $customer[0];
            
            $data["header"] = $this->load->view('/common/header', $data, true);
            $data["footer"] = $this->load->view('/common/footer', $data, true);
            $this->load->view('/usuario/edit_perfil_user', $data);
        }
    }
    
    function editPerfil() {
        
        if(empty($_SESSION["customer"])){
            $array["status"] = 'error sesion';
            $array["msg"] = 'Lo sentimos, la sesion ha sido expirada, por favor inicia sesion nuevamente.';
            echo json_encode(array ($array));
        }elseif (($_POST) || ($_FILES)) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            if (!empty($_FILES)) {
                $this->load->library('upload');
                $files = $_FILES;
                $_FILES['file']['name'] = date("YmdHis").clear($files['key_0']['name']);
                $_FILES['file']['type'] = $files['key_0']['type'];
                $_FILES['file']['tmp_name'] = $files['key_0']['tmp_name'];
                $_FILES['file']['error'] = $files['key_0']['error'];
                $_FILES['file']['size'] = $files['key_0']['size']; 

                $this->upload->initialize($this->set_upload_options("perfil")); 
                if(!$this->upload->do_upload('file')){
                    print_r($this->upload->display_errors());
                    exit();
                }

                $this->db->where('id', $_SESSION["customer"]);
                $this->db->set('imagen_perfil_file', $_FILES['file']['name']);
                $this->db->update('usuarios');  
            }
            
            
            $data = $_POST;            
            parse_str($data["info"], $info_customer);
            
            $this->db->where('id', $_SESSION["customer"]);
            $this->db->update('usuarios',$info_customer );
            
            $array["status"] = 'success';
            $array["href"] = base_url()."Usuario";
            echo json_encode(array ($array)); 
        }
    }
    
    function editPassword() {
        if(empty($_SESSION["customer"])){
            $array["status"] = 'error sesion';
            $array["msg"] = 'Lo sentimos, la sesion ha sido expirada, por favor inicia sesion nuevamente.';
            echo json_encode(array ($array));
        }elseif ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            parse_str($data["pass"][0], $info_pass);
            
            $this->db->where('id', (int)$_SESSION["customer"]);
            $customer = $this->db->get("usuarios")->result();
            if ($customer[0]->password_text == $info_pass["password_text"] ) {
                $array["status"] = 'igual';
                $array["msg"] = 'Debe colocar una contraseña diferente a la anterior.';
                echo json_encode(array ($array));
            }else{
                $this->db->where('id', $_SESSION["customer"]);
                $this->db->set('password_text', $info_pass["password_text"]);
                $this->db->update('usuarios');
                $array["status"] = 'success';
                echo json_encode(array ($array)); 
            } 
        }
    }

    private function set_upload_options($folder){   
        if (!file_exists('./uploads/'.$folder.'/')){ 
                mkdir('./uploads/'.$folder.'/', 0777, true);
        }
        $config['upload_path'] = './uploads/'.$folder.'/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000; // Maximun size 10mb
        $config['overwrite'] = true;

        return $config;
    }
      
    function cerrarSesion(){
        unset($_SESSION["customer"]);
        $array["status"] = 'success';
        echo json_encode(array ($array));
    }
}
