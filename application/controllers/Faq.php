<?php

/**
 * Description of faq
 *
 * @author David
 */
class faq extends CI_Controller{
    //put your code here
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data = [];
        if(empty($_SESSION["customer"])){
            $tables = $this->db->get("administrable_table")->result();
            foreach ($tables as $table) {
                $data["tables"][$table->name] = $this->db->get($table->name)->result();
            }
            $this->load->view('/humanoide/index', $data);
        }  else {
            
            $this->load->model('Faq_model');
            
            $faq_preguntas = $this->db->get("faq_preguntas")->result();
            $data["faqTitle"] = $faq_preguntas[0]->texto_pregunta_text;
            
            
            if(!empty($_GET["searchFAQ"])){
                $data["faq"] = $this->Faq_model->getFaqQuestionToSearch( (String)$_GET["searchFAQ"] );
            }else{
                $data["faq"] = json_decode($faq_preguntas[0]->preguntas_steps);
            }
            
            $faq_videos = $this->db->get("faq_videos")->result();
            $data["videoTitle"] = $faq_videos[0]->texto_video_text;
            $data["videos"] = json_decode($faq_videos[0]->videos_steps);
            
            $this->db->where('id', (int)$_SESSION["customer"]);
            $customer = $this->db->get("usuarios")->result();
            $data["user"] = $customer[0];
            
            $data["tab"] = "faq";
            $data["header"] = $this->load->view('/common/header', $data, true);
            $data["footer"] = $this->load->view('/common/footer', $data, true);
            $this->load->view('/faq/faq', $data);
        }
    }
}
