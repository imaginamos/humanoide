<?php
/**
 * Description of proveedor
 *
 * @author David Sánchez
 */
class proveedor extends CI_Controller {
    
    function __construct(){
        parent::__construct();
    }

    function index(){
        $data = [];
        if(empty($_SESSION["customer"])){
            $tables = $this->db->get("administrable_table")->result();
            foreach ($tables as $table) {
                $data["tables"][$table->name] = $this->db->get($table->name)->result();
            }
            $this->load->view('/humanoide/index', $data);
        }  else {
            $this->db->where('id', (int)$_SESSION["customer"]);
            $customer = $this->db->get("usuarios")->result();
            $data["user"] = $customer[0];
            
            $this->load->model('Proveedor_model');
            
            //proveedores destacados
            $data["destacados"] = $this->Proveedor_model->gerProveedorDestacados();
            $data["ciudades"] = $this->Proveedor_model->getCiudadesProveedores();
            //categorias
            $data["categorias"] = $this->db->get("proveedor_categorias")->result();
            
            if(!empty($_GET["categoria"])){
                $this->db->where('id', (int)$_GET["categoria"]);
                $data["categoria"] = $this->db->get("proveedor_categorias")->result();
                
                //$this->db->like('usuarios_multirelation', (int)$_SESSION["customer"]);
                $proveedor = $this->db->get("proveedor")->result();
                
                $serviciosFinal = [];
                foreach ($proveedor as $value) {
                    $status = false;
                    $serviciosProveedor = explode(",", $value->proveedor_servicios_multirelation);
                    foreach ($serviciosProveedor as $servicio) {
                        $this->db->where('id', $servicio);
                        $s = $this->db->get("proveedor_servicios")->result();  
                        if($s[0]->proveedor_categorias_relation == $_GET["categoria"]){
                            $status = true;
                        }
                    }   
                    if($status == true){
                        array_push($serviciosFinal, $value); 
                    }
                    
                }
                $data["proveedor"] = $serviciosFinal;
            } elseif (!empty ($_GET["ciudad"])) {
                $data["proveedor"] = $this->Proveedor_model->getProveedoresToCiudad( (String)$_GET["ciudad"] );
            } elseif (!empty ($_GET["search"])) {
                $data["proveedor"] = $this->Proveedor_model->getProveedoresToName( (String)$_GET["search"] );
            }else{
                //prooveedores
                $this->db->like('usuarios_multirelation', (int)$_SESSION["customer"]);
                $data["proveedor"] = $this->db->get("proveedor")->result();
            }
            
            $data["tab"] = "proveedor"; 
            
            $data["header"] = $this->load->view('/common/header', $data, true);
            $data["footer"] = $this->load->view('/common/footer', $data, true);
            $this->load->view('/proveedor/index', $data);
        }
    }
    
    public function viewAddProveedor() {
        $data = [];
        
        $categorias = $this->db->get("proveedor_categorias")->result();
        $arrayCategorias = [];
        foreach ($categorias as $value) {
            if($value->nombre_text != "Proveedores destacados"){
                array_push($arrayCategorias, $value);
            }
        }
        $data["categorias"] = $arrayCategorias;
        $data["servicios"] = $this->db->get("servicios_humanoide")->result();
        
        $this->db->where('id', (int)$_SESSION["customer"]);
        $customer = $this->db->get("usuarios")->result();
        $data["user"] = $customer[0];
        
        $data["header"] = $this->load->view('/common/header', $data, true);
        $data["footer"] = $this->load->view('/common/footer', $data, true);
        
        $this->load->view('/proveedor/nuevo_proveedor', $data);
    }
    
    public function viewDetalleProveedor() {
        $data = [];
        
        $this->db->where('id', (int)$_SESSION["customer"]);
        $customer = $this->db->get("usuarios")->result();
        $data["user"] = $customer[0];
        
        if(!empty($_GET["proveedor"])){
            $this->load->model('Proveedor_model');

            $this->db->where('id', (int)$_GET["proveedor"]);
            $proveedor = $this->db->get("proveedor")->result();
            $data["proveedor"] = $proveedor[0];
            
            //sedes
            $sedes = explode(",", $proveedor[0]->proveedor_sedes_multirelation);
            $infoSedes = [];
            foreach ($sedes as $value) {
                $this->db->where('id', (int)$value);
                $sede = $this->db->get("proveedor_sedes")->result();
                array_push($infoSedes, $sede);
            }
            $data["sedes"] = $infoSedes;
            
            //servicios
            $servicios = explode(",", $proveedor[0]->proveedor_servicios_multirelation);
            $categorias = [];
            foreach ($servicios as $value) {
                //get service
                $this->db->where('id', (int)$value);
                $servicio = $this->db->get("proveedor_servicios")->result();
                array_push($categorias, $servicio[0]->proveedor_categorias_relation);
            }
            
            $categoriaServices = [];
            
            foreach ($categorias as $value) {  
                //get categorias
                $this->db->where('id', $value);
                $categoria = $this->db->get("proveedor_categorias")->result();
                
                $c = [];
                $c["categoria"] = $categoria;
                foreach ($servicios as $servicio) {
                    //get service
                    $this->db->where('id', (int)$servicio);
                    $s = $this->db->get("proveedor_servicios")->result();
                    if($s[0]->proveedor_categorias_relation == $categoria[0]->id){
                       $c["servicios"][] = $s;
                    }
                }
                array_push($categoriaServices, $c);
            }
            $data["categorias"] = $categoriaServices;
        }        
        
        $data["header"] = $this->load->view('/common/header', $data, true);
        $data["footer"] = $this->load->view('/common/footer', $data, true);
        
        if(!empty($_GET["mode"]) && ($_GET["mode"] == "edit")){
            $data["categorias"] = $this->Proveedor_model->getCategoriasNoDestacado();
            $data["serviciosHumanoide"] = $this->Proveedor_model->getServiciosHumanoide();
            $data["serviciosProveedor"] = $this->Proveedor_model->getServiciosProveedor($_GET["proveedor"]);
            $this->load->view('/proveedor/detalle_proveedor_edit', $data);
        }else{
            $this->load->view('/proveedor/detalle_proveedor', $data);
        }
        
        
    }
    
    public function addProveedor() {
        if(empty($_SESSION["customer"])){
            $array["status"] = 'error sesion';
            $array["msg"] = 'Lo sentimos, la sesion ha sido expirada, por favor inicia sesion nuevamente.';
            echo json_encode(array ($array));
        }elseif ($_POST) {
	    //check if its an ajax request, exit if not
	    if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
	        //exit script outputting json data
	        $output = json_encode(
	                array(
	                    'type' => 'error',
	                    'text' => 'Request must come from Ajax'
	        ));
	        die($output);
	    }
            
            $data = $_POST;
            
            //proveedor
            parse_str($data["proveedor"], $info_proveedor);
            
            $proveedor["nombre_text"] = $info_proveedor["nombre_text"];
            $proveedor["correo_text"] = $info_proveedor["correo_text"];
            $proveedor["NIT_number"] = $info_proveedor["NIT_number"];
            //$proveedor["persona_contacto_text"] = $info_proveedor["persona_contacto_text"];
            $proveedor["codigo_humanoide_number"] = date('Y-m-d-h:i:s')."/".$info_proveedor["NIT_number"];
            $proveedor["usuarios_multirelation"] = $_SESSION["customer"];
            $proveedor["created_at"] = date('Y-m-d h:i:s');
            
            //servicios
            $arrayServicios = json_decode($data["servicios"]);
            $services = [];
            foreach ($arrayServicios as $servicio) {
                $services[] = (array)json_decode($servicio);
            }
            for ($i = 0; $i < count($services); $i++) {
                $services[$i]["precio_desde_number"] = explode(" - ", str_replace("$", "", $services[$i]["precio"]))[0];
                $services[$i]["precio_hasta_number"] = explode(" - ", str_replace("$", "", $services[$i]["precio"]))[1];
                unset($services[$i]["precio"]);
            }
            
            //sedes
            $arraySedes = json_decode($data["sedes"]);
            $sedes = [];
            foreach ($arraySedes as $sede) {
                $sedes[] = (array)json_decode($sede);
            }
            
            for ($i = 0; $i < count($sedes); $i++) {
                $sedes[$i]["pos_map"] = $sedes[$i]["latitud"].",".$sedes[$i]["longitud"];
                if($i == 0){
                    $sedes[$i]["sede_principal_checkbox"] = "Si";
                }
                unset($sedes[$i]["latitud"],$sedes[$i]["longitud"]);
            }
            
            $this->db->insert("proveedor",$proveedor);
            $idProveedor = $this->db->insert_id();
            
            //retorna id de los servicios
            $servicesIds = [];
            foreach ($services as $value) {
                $this->db->insert("proveedor_servicios",$value);
                array_push($servicesIds, $this->db->insert_id());
            }
            
            //retorna id de las sedes
            $sedesIds = [];
            foreach ($sedes as $value) {
                $this->db->insert("proveedor_sedes",$value);
                array_push($sedesIds, $this->db->insert_id());
            }
            
            //actualizar servicios y sedes en el proveedor;
            $this->db->set('proveedor_servicios_multirelation', implode(",", $servicesIds));
            $this->db->set('proveedor_sedes_multirelation', implode(",", $sedesIds));
            $this->db->where('id', $idProveedor);
            $this->db->update('proveedor');
            
            //actualizar foto de perfil del proveedor
            if (!empty($_FILES)) {
                $this->load->library('upload');
                $files = $_FILES;
                $_FILES['file']['name'] = date("YmdHis").clear($files['key_0']['name']);
                $_FILES['file']['type'] = $files['key_0']['type'];
                $_FILES['file']['tmp_name'] = $files['key_0']['tmp_name'];
                $_FILES['file']['error'] = $files['key_0']['error'];
                $_FILES['file']['size'] = $files['key_0']['size']; 

                $this->upload->initialize($this->set_upload_options("files")); 
                if(!$this->upload->do_upload('file')){
                    print_r($this->upload->display_errors());
                    exit();
                }

                $this->db->where('id', $idProveedor);
                $this->db->set('imagen_file', $_FILES['file']['name']);
                $this->db->update('proveedor');  
            }
            
            $array["status"] = 'success';
            $array["href"] = base_url()."/Proveedor/";
            echo json_encode(array ($array));
        }
    }
    
    function addProveedorDestacado() {
        if($_POST){
            $data = $_POST;
            if(!empty($data["proveedor"])){
                $this->db->where('id', (int)$data["proveedor"]);
                $proveedor = $this->db->get("proveedor")->result();
                
                $this->db->where('id', $proveedor[0]->id);
                $this->db->like('usuarios_multirelation', (int)$_SESSION["customer"]);
                $status = $this->db->get("proveedor")->result();
                if($status){
                    $array["status"] = 'error';
                    $array["msg"] = "El proveedor ya ha sido vinculado anteriormente.";
                    echo json_encode(array ($array));
                }else{
                    if ( ($proveedor[0]->usuarios_multirelation == "") || ($proveedor[0]->usuarios_multirelation == null)) {
                        $usuario = $_SESSION["customer"];
                    }else{
                        $usuario = $proveedor[0]->usuarios_multirelation.",".$_SESSION["customer"];
                    }
                    $this->db->set('usuarios_multirelation', $usuario);
                    $this->db->where('id', $proveedor[0]->id);
                    $this->db->update('proveedor');

                    $array["status"] = 'success';
                    $array["href"] = base_url()."Proveedor/";
                    echo json_encode(array ($array));
                }
            }
        }
    }
    
    function updateProveedor() {
        if($_POST){
            $data = $_POST;
            
            if(!empty($data["proveedor"])){
                $this->load->model('Proveedor_model');
                
                parse_str($data["proveedor"], $info_proveedor);                
                $proveedor = $this->Proveedor_model->getProveedorById($info_proveedor["idProveedor"]);
                
                unset($info_proveedor["idProveedor"]);
                $this->Proveedor_model->updateProveedor($proveedor[0]->id,$info_proveedor);
                
                //servicios
                $arrayServicios = json_decode($data["servicios"]);
                $services = [];
                foreach ($arrayServicios as $servicio) {
                    $infoService = (array)json_decode($servicio);
                    $idService = $infoService["id"];
                    unset($infoService["id"]);
                    $this->Proveedor_model->updateService($idService,$infoService);
                }
                
                //sedes
                $arraySedes = json_decode($data["sedes"]);
                $sedes = [];
                foreach ($arraySedes as $sede) {
                    $infoSede = (array)json_decode($sede);
                    $infoSede["pos_map"] = $infoSede["latitud"].",".$infoSede["longitud"];
                    $idSede = $infoSede["id"];
                    unset($infoSede["id"],$infoSede["latitud"],$infoSede["longitud"]);
                    $this->Proveedor_model->updateSede($idSede,$infoSede);
                }
                
                //actualizar foto de perfil del proveedor
            if (!empty($_FILES)) {
                $this->load->library('upload');
                $files = $_FILES;
                $_FILES['file']['name'] = date("YmdHis").clear($files['key_0']['name']);
                $_FILES['file']['type'] = $files['key_0']['type'];
                $_FILES['file']['tmp_name'] = $files['key_0']['tmp_name'];
                $_FILES['file']['error'] = $files['key_0']['error'];
                $_FILES['file']['size'] = $files['key_0']['size']; 

                $this->upload->initialize($this->set_upload_options("files")); 
                if(!$this->upload->do_upload('file')){
                    print_r($this->upload->display_errors());
                    exit();
                }

                $this->db->where('id', $proveedor[0]->id);
                $this->db->set('imagen_file', $_FILES['file']['name']);
                $this->db->update('proveedor');  
            }
            
                $array["status"] = 'success';
                $array["href"] = base_url()."proveedor/viewDetalleProveedor?proveedor=". $proveedor[0]->id;
                echo json_encode(array ($array));
            }
        }
    }
    
    private function set_upload_options($folder){   
        if (!file_exists('./uploads/'.$folder.'/')){ 
                mkdir('./uploads/'.$folder.'/', 0777, true);
        }
        $config['upload_path'] = './uploads/'.$folder.'/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000; // Maximun size 10mb
        $config['overwrite'] = true;

        return $config;
    }
}
